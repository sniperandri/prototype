<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterUkd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_ukd', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_ukd')->nullable();
            $table->string('kode_ukd')->nullable();
            $table->string('ketua_ukd')->nullable();
            $table->string('alamat_ukd')->nullable();
            $table->string('telepon_ukd')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_ukd');
    }
}
