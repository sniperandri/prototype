<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterUkd extends Model
{
    protected $table = 'master_ukd';
    protected $fillable = ['_token', 'nama_ukd', 'alamat_ukd', 'kode_ukd', 'telepon_ukd', 'ketua_ukd'];
}
