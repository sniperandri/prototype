<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Permohonan;

class User extends Authenticatable
{
    use Notifiable;

    protected $dates = ['approved_time'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','email','password','jenis_kelamin','jenis_user','nama_perusahaan','no_hp','ktp','foto','nik','role','status_email','status_user','last_login','alamat','jenis_identitas', 'no_identitas', 'unit', 'file_identitas', 'file_ijazah', 'file_foto', 'pendidikan_terakhir'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function approved($user_id){
        $approvedBy = User::find($user_id);
        if($approvedBy){
            return $approvedBy->name;
        }else{
            return '';
        }
    }

    public function when($user_id){
        $when = User::find($user_id);
        if($when){
            return $when->approved_time;
        }else{
            return '';
        }
    }

    public function ownsPermohonan(Permohonan $permohonan){
        return auth()->id() == $permohonan->user_id;
    }
}
