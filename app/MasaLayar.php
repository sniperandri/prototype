<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasaLayar extends Model
{
    protected $table = 'masa_layar';
    protected $fillable = ['nama_kapal','daya','tonase_kotor','tgl_naik','tgl_turun','no_buku_pelaut','seafarer_code','daerah_layan','jabatan'];
}
