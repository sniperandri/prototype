<?php

namespace App\Policies;

use App\User;
use App\Permohonan;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermohonanPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function cetakBukti(User $user, Permohonan $permohonan){
        return $user->ownsPermohonan($permohonan);
    }

    public function cetakSk(User $user, Permohonan $permohonan){
        if($user->role != 'PJ'){
            return true;
        }else{
            if($user->ownsPermohonan($permohonan) && $permohonan->status == 4){
                return true;
            }else{
                return false;
            }
        }        
    }
}
