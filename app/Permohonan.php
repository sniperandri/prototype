<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permohonan extends Model
{
	protected $table  = 'permohonan';
	protected $guards = [];
	protected $dates = ['approved_time'];

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function approved(){
		return $this->belongsTo(User::class,'approved_by','id');
	}

	public function masaLayar(){
		return $this->belongsTo(Permohonan::class,'seafarer_code','seafarer_code');
	}

	public function kodbil($param){
		$kodbil = KodeBilling::where('permohonan_id',$param)->latest()->first();
		if($kodbil){
			return $kodbil;
		}else{
			return '';
		}
	}

	public function kodebilling(){
		$kodbil = KodeBilling::where('permohonan_id',$this->id)->latest()->first();
		if($kodbil){
			$split = str_split($kodbil->kode_billing_simponi,3);
			$stringKodbil = '';
			foreach($split as $digit){
			  $stringKodbil .= $digit . ' ';
			} 
			return $stringKodbil;
		}else{
			return '';
		}

	}
}
