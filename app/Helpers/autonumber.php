<?php 

	function integerToRoman($integer)
	{
	 // Convert the integer into an integer (just to make sure)
	 $integer = intval($integer);
	 $result = '';
	 
	 // Create a lookup array that contains all of the Roman numerals.
	 $lookup = array('M' => 1000,
	 'CM' => 900,
	 'D' => 500,
	 'CD' => 400,
	 'C' => 100,
	 'XC' => 90,
	 'L' => 50,
	 'XL' => 40,
	 'X' => 10,
	 'IX' => 9,
	 'V' => 5,
	 'IV' => 4,
	 'I' => 1);
	 
	 foreach($lookup as $roman => $value){
	  // Determine the number of matches
	  $matches = intval($integer/$value);
	 
	  // Add the same number of characters to the string
	  $result .= str_repeat($roman,$matches);
	 
	  // Set the integer to be the remainder of the integer and the value
	  $integer = $integer % $value;
	 }
	 
	 // The Roman numeral should be built, return it
	 return $result;
	}

	function autonumberSertifikat($lastNomorSertifikat){
		if($lastNomorSertifikat){
			$tahun = date('y');
			$tahun0 = explode('/',$lastNomorSertifikat)[3];
			$tahun0 = explode('-',$tahun0)[1];
			
			
			if($tahun === $tahun0){
				$bulan = integerToRoman(date('m'));
				$bulan0 = explode('/',$lastNomorSertifikat)[2];
				if($bulan === $bulan0){
					$lastNumber = explode('/',$lastNomorSertifikat)[1];
					$nextNumber = $lastNumber+1;
					$nextNumber = sprintf("%04s", $nextNumber);
				}else{
					$nextNumber = '0001';
				}
			}else{
				$bulan = integerToRoman(01);
				$nextNumber = '0001';
			}

			$nextSertifikatNumber = 'No.AL.506/'.$nextNumber.'/'.$bulan. '/Syb.Tpk-'.$tahun;

			return $nextSertifikatNumber;
		}else{
			echo "Nomor Sertifikat Tidak Ditemukan";die();
		}
	}

	function randomTen(){
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		   $result = '';
		   for ($i = 0; $i < 10; $i++)
		       $result .= $characters[mt_rand(0, 61)];
		return $result;
	}
?>
