<?php 
	function datediff($date1,$date2){
		$newDate = new Datetime($date1);
		$oldDate = new Datetime($date2);
		$interval = date_diff($newDate, $oldDate); 
        return $interval->format('%y tahun %m bulan %d hari'); 
	}

	function datediffinterval($date1,$date2){
		$newDate = new Datetime($date1);
		$oldDate = new Datetime($date2);
		$interval = date_diff($newDate, $oldDate); 
        return $interval; 
	}

	function datediffToYear($date1,$date2){
		$newDate = new Datetime($date1);
		$oldDate = new Datetime($date2);
		$interval = date_diff($newDate, $oldDate); 
		$interval = $interval->format('%y');
		// $interval = sprintf("%02s", $interval);
        return $interval;
	}

	function datediffToMonth($date1,$date2){
		$newDate = new Datetime($date1);
		$oldDate = new Datetime($date2);
		$interval = date_diff($newDate, $oldDate); 
		$interval = $interval->format('%m'); 
		// $interval = sprintf("%02s", $interval);
        return $interval; 
	}

	function datediffToDay($date1,$date2){
		$newDate = new Datetime($date1);
		$oldDate = new Datetime($date2);
		$interval = date_diff($newDate, $oldDate); 
		$interval = $interval->format('%d'); 
		// $interval = sprintf("%02s", $interval);
        return $interval; 
	}

	function dateTimeToMonth($datetime){
		$dt = new DateTime($datetime);

		$date = $dt->format('M');
		
		return $date;
	}

	function dateTimeToDate($datetime){
		$dt = new DateTime($datetime);

		$date = $dt->format('d M Y');
		
		return $date;
	}

	function dateTimeToDate2($datetime){
		$dt = new DateTime($datetime);

		$date = $dt->format('Y-m-d');
		
		return $date;
	}

	function dateTimeToDate3($datetime){
		$dt = new DateTime($datetime);

		$date = $dt->format('d M Y H:i:s');
		
		return $date;
	}

	function dateTimeToDate4($datetime){
		$dt = new DateTime($datetime);

		$date = $dt->format('d-m-Y');
		
		return $date;
	}

	function numberMapping($number){
		$numbers = ['nol','satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan','sepuluh','sebelas','dua belas', 'tiga belas', 'empat belas','lima belas', 'enam belas', 'tujuh belas', 'delapan belas', 'sembilan belas', 'dua puluh', 'dua puluh satu', 'dua puluh dua', 'dua puluh tiga', 'dua puluh empat', 'dua puluh lima', 'dua puluh enam', 'dua puluh tujuh', 'dua puluh delapan', 'dua puluh sembilan', 'tiga puluh', 'tiga puluh satu'];
		return $numbers[$number];
	}

?>
