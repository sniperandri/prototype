<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KodeBilling extends Model
{
    protected $table = 'kode_billing';
	protected $guards = [];
	protected $dates = ['expired_date','date_sent','br_expired_date','pr_trx_date','created_at','updated_at'];

	public function permohonan(){
		return $this->belongsTo(Permohonan::class,'permohonan_id','id');
	}

}
