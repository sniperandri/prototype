<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                => 'required',
            'email'               => 'required|email|unique:users',
            'password'            => 'required|min:6',
            'jenis_identitas'     => 'required',
            'no_identitas'        => 'required',
            'unit'                => 'required',
            'file_identitas'      => 'required',
            'file_ijazah'         => 'required',
            'file_foto'           => 'required',
            'pendidikan_terakhir' => 'required'
        ];
    }

    public function messages()
    {
        $atribut = ucwords(':attribute');

        return [
            'required'     => $atribut . ' tidak boleh kosong.',
            'email'        => ':attribute harus berupa email yang valid.', 
            'password.min' => 'Password minimal 6 karakter.',
        ];
    }
}
