<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permohonan;
use App\Http\Requests\PermohonanStoreRequest; 
use Auth;
use Session;
use App\KodeBilling;
use App\Http\Controllers\PnbpController;
use DateTime;

class PermohonanSkLayarController extends Controller
{
    const PERMOHONAN_DIPROSES = 1;
    const PERMOHONAN_DITERIMA = 2;
    const PNBP_DIBAYAR = 4;

    public function index(){
    	$permohonans = Permohonan::latest()->where('user_id',Auth::user()->id)->paginate(25);

        //Cek pembayaran
        foreach($permohonans as $permohonan){
            if($permohonan->status < self::PNBP_DIBAYAR){
                $simponi = Kodebilling::where('permohonan_id',$permohonan->id)->latest()->first();
                if($simponi){
                    $today = date('Y-m-d H:i:s');
                    if($simponi->expired_date < $today){
                        $simponi->is_expired = 1;
                        $simponi->remark = 'EXPIRED';
                        $simponi->save();
                        $permohonan->status = self::PERMOHONAN_DITERIMA;
                        $permohonan->save();
                    }else{
                        //Cek Status Pembayaran
                        $header = [];
                        $header['trx_id'] = $simponi->trx_id;
                        $header['invoice_no'] = $simponi->invoice_no;
                        $header['kode_billing_simponi'] = $simponi->kode_billing_simponi;

                        $response = PnbpController::BillingStatus($header);
                        if($response['ResponseCode'] == '00'){
                            if(empty($simponi->pr_ntb) && empty($simponi->pr_ntpn)){
                                $simponi->br_simponi_trx_id = $response['ResponseData']['SimponiTrxId'];
                                $simponi->pr_ntb = $response['ResponseData']['NTB'];
                                $simponi->pr_ntpn = $response['ResponseData']['NTPN'];
                                $simponi->pr_trx_date = $response['ResponseData']['TrxDate'];
                                $simponi->pr_bank_persepsi = $response['ResponseData']['BankPersepsi'];
                                $simponi->pr_channel_pembayaran = $response['ResponseData']['ChannelPembayaran'];
                                $permohonan->status = 4;
                                $permohonan->save();
                                $simponi->save();
                            }
                        }
                    }
                }
            }
        }
    	return view('sk/index', compact('permohonans'));
    }

    public function create(){
        $pemohon = Auth::user();
        $pelaut = $this->getPelaut($pemohon->seafarer_code);

    	return view('sk/form',compact('pemohon','pelaut'));
    }

    protected function getPelaut($code){
        header('Access-Control-Allow-Origin: *');
        $url = "https://pelaut.dephub.go.id/webapp/finddata/1/{$code}";
        $response = file_get_contents($url);
        $data = json_decode($response,true);
        return $data;
    }

    public function store(PermohonanStoreRequest $request){
    	// $latestNumber = Permohonan::orderBy('id','desc')->first();

     //    if(!$latestNumber){
     //        $latestNumber = 'No.AL.506/0000/I/Syb.Tpk-19';
     //    }else{
     //        $latestNumber = $latestNumber->nomor_permohonan;
     //    }
    	$data = $request->all();
        $bulan = date('m');
        $tahun = date('Y');

        if($request->hasFile('file_upload')){
            $path = $request->file('file_upload')->store('file-permohonan/'.$tahun.'/'.$bulan);
            $data['file_upload'] = $path;
        }

        $permohonan = new Permohonan();
        $permohonan->user_id = Auth::user()->id;
        $permohonan->seafarer_code = Auth::user()->seafarer_code;
        if($permohonan->file_upload){
            $permohonan->file_upload = $data['file_upload'];
        }
        // $permohonan->nomor_permohonan = autonumberSertifikat($latestNumber);
        $permohonan->nomor_permohonan = time();

        $permohonan->keperluan = $data['keperluan'];
        $permohonan->status = self::PERMOHONAN_DIPROSES;
        $permohonan->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Permohonan Berhasil Ditambahkan."
        ]);

        return view('sk.notifikasi',compact('permohonan'));
    }

}
