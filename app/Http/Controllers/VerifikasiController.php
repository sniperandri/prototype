<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permohonan;
use App\MasaLayar;
use App\User;
use Session;
use Auth;
use App\Mail\EmailVerification;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class VerifikasiController extends Controller
{
	const PERMOHONAN_DITERIMA = 2;
	const PERMOHONAN_DITOLAK = 0;

    public function verifikasiSk(){
    	$permohonans = Permohonan::latest()->paginate(25);
    	return view('verifikasi/sk/index', compact('permohonans'));
    }

    public function verifikasiUser(){
    	$registrar = User::where('role','PJ')->latest()->paginate(25);
        return view('verifikasi.user.index', compact('registrar'));
    }

    public function proses(Request $request, $id){
        $permohonan = Permohonan::findOrFail($id);
        $pelaut = $this->getPelaut($permohonan->seafarer_code);

        if($pelaut){
            return view('verifikasi.sk.proses',compact('permohonan','pelaut'));
        }else{
            abort(404); 
        }
    }

    protected function getPelaut($code){
        header('Access-Control-Allow-Origin: *');
        $url = "https://pelaut.dephub.go.id/webapp/finddata/1/{$code}";
        $response = file_get_contents($url);
        $data = json_decode($response,true);
        return $data;
    }

    protected function getBukuPelaut($code){
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );  
        header('Access-Control-Allow-Origin: *');
        $url = "https://dokumenpelaut.dephub.go.id/ws/sijil?sc={$code}";
        $response = file_get_contents($url, false, stream_context_create($arrContextOptions));
        $data = json_decode($response,true);
        return $data;
    }

    public function approve(Request $request, $id){
    	$permohonan = Permohonan::findOrFail($id);
        $pelaut = $this->getPelaut($permohonan->seafarer_code);
        $permohonans = MasaLayar::where('seafarer_code',$permohonan->seafarer_code)->get();

        // $buku_pelaut = $this->getBukuPelaut('1201567968');
        $buku_pelaut = $this->getBukuPelaut($permohonan->seafarer_code);

    	return view('masa-layar.index', compact('permohonan','permohonans','pelaut','buku_pelaut'));
    }

    public function approveSk(Request $request, $id){
        $permohonan = Permohonan::findOrFail($id);

        $latestNumber = Permohonan::orderBy('id','desc')->first();

        if(!$latestNumber->nomor_sertifikat){
            $latestNumber = 'No.AL.506/0000/I/Syb.Tpk-19';
        }else{
            $latestNumber = $latestNumber->nomor_sertifikat;
        }

        if($permohonan->status < self::PERMOHONAN_DITERIMA && $permohonan->status !=0){
            $permohonan->status = self::PERMOHONAN_DITERIMA;
        }

        $permohonan->approved_by = Auth::user()->id;
        $permohonan->approved_time = date('Y-m-d H:i:s');
        $permohonan->nomor_sertifikat = autonumberSertifikat($latestNumber);

        $permohonan->save();
        $permohonans = Permohonan::latest()->get();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Permohonan Berhasil Di Approve."
        ]);

        return view('verifikasi.sk.index',compact('permohonans'));
    }

    public function reject(Request $request, $id){
        $permohonan = Permohonan::findOrFail($id);
        $permohonan->status = self::PERMOHONAN_DITOLAK;

        $permohonan->approved_by = Auth::user()->id;
        $permohonan->approved_time = date('Y-m-d H:i:s');

        $permohonan->save();
        $permohonans = Permohonan::latest()->get();

        Session::flash("flash_notification", [
            "level" => "danger",
            "message" => "Permohonan Ditolak."
        ]);

        return view('verifikasi.sk.index',compact('permohonans'));
    }

    public function approveUser(Request $request, $id){
        $user = User::findOrFail($id);

        $user->approved_by = Auth::user()->id;
        $user->approved_time = date('Y-m-d H:i:s');

        $user->status_user = self::PERMOHONAN_DITERIMA;

        try {
            $user->status_email = 2;
            $user->save();
            Mail::to($user->email)->send(new EmailVerification($user));
        } catch (\Exception $e) {
            // echo $e->getMessage();
            echo "<pre>";
            print_r($e->getMessage());
            echo "</pre>";
            die();
            $user->status_email = 0;
            // $user->save();
        }

        return redirect()->route('verifikasi-user');
    }

    public function rejectUser(Request $request, $id){
    	$user = User::findOrFail($id);
    	$user->status_user = self::PERMOHONAN_DITOLAK;
    	$user->save();

    	return redirect()->route('verifikasi-user');
    }

    public function lihat(Request $request, $id){
        $permohonan = Permohonan::findOrFail($id);
        $pelaut = $this->getPelaut($permohonan->seafarer_code);
        $permohonans = MasaLayar::where('seafarer_code',$permohonan->seafarer_code)->get();

	$buku_pelaut = $this->getBukuPelaut($permohonan->seafarer_code);
	// $buku_pelaut = $this->getBukuPelaut('1201567968');
	
        return view('masa-layar.index', compact('permohonan','permohonans','pelaut','buku_pelaut'));
    }

    public function search(Request $request){
        $no_permohonan = $request->get('search');

        $permohonans = Permohonan::latest()->where('nomor_permohonan',$no_permohonan)->paginate(25);

        $role = Auth::user()->role;
        if(strtoupper($role) == 'PJ'){
            return view('sk/index', compact('permohonans'));
        }else{
            return view('verifikasi/sk/index', compact('permohonans'));
        }

    }

    public function searchUser(Request $request){
        $name = $request->get('search');

        $registrar = User::latest()->where('name', 'like', '%' . $name . '%')->paginate(25);

        return view('verifikasi.user.index', compact('registrar'));
    }
}
