<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterUkd;
use Illuminate\Support\Facades\Hash;
use Session;

class MasterUkdController extends Controller
{
    public function index(){
    	$masterUkd = MasterUkd::all();
    	return view('master-ukd.index', compact('masterUkd'));
    }

    public function create(){
    	return view('master-ukd.form');
    }

    public function edit($id)
    {
        $masterUkd = MasterUkd::findOrFail($id);
        return view('master-ukd.edit',compact('masterUkd'));
    }

    public function update(Request $request, $id)
    {
        $user = MasterUkd::findOrFail($id);
        $data = request()->except(['_token']);;

        $user->update($data);

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Master UKD berhasil diupdate."
        ]);

        return redirect()->route('master-ukd.index');
    }

    public function destroy($id)
    {
        $user = MasterUkd::findOrFail($id);
        $user->delete();

        return redirect()->route('master-ukd.index');
    }

    public function store(Request $request){
    	$data = request()->except(['_token']);
        $data['kode_ukd'] = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $data['nama_ukd'])));
    	MasterUkd::create($data);

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Master UKD berhasil ditambahkan."
        ]);

    	return redirect()->route('master-ukd.index');
    }
}
