<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Session;

class PetugasController extends Controller
{
    public function index(){
    	$petugas = User::where('role','<>','PJ')->get();
    	return view('petugas.index', compact('petugas'));
    }

    public function create(){
    	return view('petugas.form');
    }

    public function edit($id)
    {
        $petugas = User::findOrFail($id);
        return view('petugas.edit',compact('petugas'));
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $data = $request->all();

        $user->update($data);

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Petugas berhasil diupdate."
        ]);

        return redirect()->route('petugas.index');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('petugas.index');
    }

    public function store(Request $request){
    	$data = $request->all();
    	$data['status_user'] = 2;
    	$data['status_email'] = 2;
        $data['password'] = Hash::make($data['password']);
        $data['jenis_user'] = 'PETUGAS';
    	User::create($data);

    	return redirect()->route('petugas.index');
    }
}
