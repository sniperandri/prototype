<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterStoreRequest;
use App\User;
use App\Mail\EmailRegistration;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Session;

class RegisterController extends Controller
{
    public function index()
    {
        return view('auth/register');
    }

    protected function getPelaut($code){
        ini_set('default_socket_timeout', 10);
        header('Access-Control-Allow-Origin: *');
        $url = "https://pelaut.dephub.go.id/webapp/finddata/1/{$code}";
        $response = @file_get_contents($url);

        $data = json_decode($response,true);
        return $data;        
    }

    public function store(RegisterStoreRequest $request){
    	$data = $request->all();

        $bulan = date('m');
        $tahun = date('Y');

        if($request->hasFile('file_identitas')){
            $path = $request->file('file_identitas')->store('file-permohonan/'.$tahun.'/'.$bulan);
            $data['file_identitas'] = $path;
        }

        if($request->hasFile('file_ijazah')){
            $path = $request->file('file_ijazah')->store('file-permohonan/'.$tahun.'/'.$bulan);
            $data['file_ijazah'] = $path;
        }

        if($request->hasFile('file_foto')){
            $path = $request->file('file_foto')->store('file-permohonan/'.$tahun.'/'.$bulan);
            $data['file_foto'] = $path;
        }

        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password      = Hash::make($data['password']);
        $user->role          = $data['role'];
        $user->jenis_identitas = $data['jenis_identitas'];
        $user->no_identitas = $data['no_identitas'];
        $user->unit = $data['unit'];
        $user->pendidikan_terakhir = $data['pendidikan_terakhir'];

        if($data['file_identitas']){
            $user->file_identitas = $data['file_identitas'];
        }
        if($data['file_ijazah']){
            $user->file_ijazah = $data['file_ijazah'];
        }
        if($data['file_foto']){
            $user->file_foto = $data['file_foto'];
        }

        //kirim email
        try {
            $user->status_email = 1;
            $user->save();
            Mail::to($user->email)->send(new EmailRegistration($user));
        } catch (\Exception $e) {
            // echo $e->getMessage();
            echo "<pre>";
            print_r($e->getMessage());
            echo "</pre>";
            die();
            $user->status_email = 0;
            // $user->save();
        }

        return redirect()->route('pesan');  
    }

    public function pesan(){
        return view('auth/pesan');
    }

    public function verifikasi($id){
        $user = User::where('id', $id)->first();
        $user->status_email = 2;
        $user->status_user = 1;
        $user->save();
        return redirect()->route('terimakasih');
    }

    public function sukses(){
        return view('auth/terimakasih');
    }
}
