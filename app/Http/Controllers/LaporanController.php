<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permohonan;

class LaporanController extends Controller
{
    public function index(){
    	$permohonans = Permohonan::where('status','<>',1)->latest()->get();
    	return view('laporan.index', compact('permohonans'));
    }

    public function search(Request $request){
        $dari = $request->get('dari');
        $sampai = $request->get('sampai');
        $permohonans = Permohonan::where('approved_time','>=', $dari)
                    ->where('approved_time','<',$sampai)
                    ->where('status','<>',1)
                    ->get();

        return view('laporan.index', ['permohonans'=>$permohonans]);
    }
}
