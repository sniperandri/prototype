<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasaLayar;
use App\Permohonan;

class MasaBerlayarController extends Controller
{
    public function create($code){
    	return view('masa-layar/form',compact('code'));
    }

    public function store(Request $request){
    	$permohonans = Permohonan::latest()->get();
    	$id = $permohonans[0]->id;

    	$data = $request->all();

    	MasaLayar::create($data);

    	return redirect()->route('permohonan.approve', ['id' => $id]);
    }
}
