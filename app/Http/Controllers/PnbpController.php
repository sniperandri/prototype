<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SoapClient;
use App\Permohonan;
use Auth;
use App\KodeBilling;
use Session;

class PnbpController extends Controller
{
	public $url = 'http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl';
    // public $url = 'http://10.47.210.17:7800/SimponiBRI_Service?wsdl';

    // CONST APPS_ID = '000';
    CONST APPS_ID = '025';
    CONST ROUTE_ID = '001';
    CONST USER_ID = '0';
    CONST PASSWORD = '0';
    CONST KODE_KL = '022';
    CONST KODE_ESELON_1 = '04';
    // CONST KODE_SATKER = '413422';
    CONST KODE_SATKER = '412781';
    CONST JENIS_PNBP = 'F';
    CONST KODE_MATA_UANG = '1';
    const GET_KODE_BILLING = 3;

    protected function PaymentRequest($invoiceNo, $header, $detail) {
        $client = new SoapClient($this->url);

        $req = $client->__call('PaymentRequest', [
            [
                'appsId' => self::APPS_ID,
                'invoiceNo' => $invoiceNo,
                'routeId' => self::ROUTE_ID,
                'data' => [
                    'PaymentHeader' => [
                        'TrxId' => $header['trx_id'],
                        'UserId' => self::USER_ID,
                        'Password' => self::PASSWORD,
                        'ExpiredDate' => $header['expired_date'],
                        'DateSent' => $header['create_date'],
                        'KodeKL' => self::KODE_KL,
                        'KodeEselon1' => self::KODE_ESELON_1,
                        'KodeSatker' => self::KODE_SATKER,
                        'JenisPNBP' => self::JENIS_PNBP,
                        'KodeMataUang' => self::KODE_MATA_UANG,
                        'TotalNominalBilling' => $header['total_nominal_billing'],
                        'NamaWajibBayar' => $header['nama_wajib_bayar']
                    ],
                    'PaymentDetails' => $detail
                ]
            ]
                ]
        );
        if ($req) {
            return $req->response;
        } else {
            return false;
        }
    }

    public static function BillingStatus($dataBilling) {
        try {
            $client = new SoapClient('http://soadev.dephub.go.id:7800/SimponiBRI_Service?wsdl');
            $req = $client->__call('BillingStatus', [
                [
                    'appsId' => self::APPS_ID,
                    'invoiceNo' => $dataBilling['invoice_no'],
                    'routeId' => self::ROUTE_ID,
                    'TrxId' => $dataBilling['trx_id'],
                    'UserId' => self::USER_ID,
                    'Password' => self::PASSWORD,
                    'KodeBillingSimponi' => $dataBilling['kode_billing_simponi'],
                    'KodeKL' => self::KODE_KL,
                    'KodeEselon1' => self::KODE_ESELON_1,
                    'KodeSatker' => self::KODE_SATKER
                ]
                    ]
            );

            return [
                'ResponseCode' => $req->ResponseCode,
                'ResponseMessage' => $req->ResponseMessage,
                'ResponseData' => [
                    'SimponiTrxId' => $req->ResponseData->SimponiTrxId,
                    'NTB' => $req->ResponseData->NTB,
                    'NTPN' => $req->ResponseData->NTPN,
                    'TrxDate' => $req->ResponseData->TrxDate,
                    'BankPersepsi' => $req->ResponseData->BankPersepsi,
                    'ChannelPembayaran' => $req->ResponseData->ChannelPembayaran,
                ]
            ];
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function kodbil(){
        $invoiceNo = 'INV.IDJKT.1904.434236';
        $header['trx_id'] = 'aR45erJsrh';
        $header['expired_date'] = '2019-05-30 23:00:00';
        $header['create_date'] = '2019-05-07 10:00:00';
        $header['total_nominal_billing'] = 10000;
        $header['nama_wajib_bayar'] = 'PT. GUNA DHARMA';
        $detail[] = [
                        'NamaWajibBayar' => 'PT. GUNA DHARMA',
                        'KodeTarifSimponi' => '001202',
                        // 'KodeTarifSimponi' => '000401',
                        'KodePPSimponi' => '201615C',
                        // 'KodeAkun' => '423218',
                        'KodeAkun' => '425515',
                        'TarifPNBP' => 10000,
                        // 'TarifPNBP' => 90,
                        'Volume' => 1,
                        // 'Satuan' => 'Per GT/15 Hari',
                        'Satuan' => 'per surat',
                        // 'TotalTarifPerRecord' => 90
                        'TotalTarifPerRecord' => 10000
                    ];
    	$response = $this->PaymentRequest($invoiceNo, $header, $detail);

        // $header = [];
        // $header['trx_id'] = 'aR45erJsrg';
        // $header['invoice_no'] = 'INV.IDJKT.1904.434235';
        // $header['kode_billing_simponi'] = '820190508464307';

        // $response = $this->BillingStatus($header);
        echo "<pre>";
        print_r($response);
        echo "</pre>";
        die();
    }

    public function getKodeBilling($permohonan){
        $permohonans = Permohonan::where('user_id',Auth::user()->id)->get();
        $perm = Permohonan::find($permohonan);
        $simponi = Kodebilling::where('permohonan_id',$perm->id)->latest()->first();
        if(!empty($simponi)){ 
            $simponi = $simponi->kode_billing_simponi;
        }else{
            $simponi = '';
        }

        $perm->status = SELF::GET_KODE_BILLING;
        $perm->save();


        $kodbil = new KodeBilling();
        $kodbil->permohonan_id = $perm->id;
        $kodbil->trx_id = randomTen();
        $kodbil->expired_date = date('Y-m-d H:i:s', strtotime('+3 days', time()));
        $kodbil->date_sent = date('Y-m-d H:i:s');
        $kodbil->nama_wajib_bayar = $perm->user->name;
        $kodbil->volume = 1;
        $kodbil->satuan = 'per surat';
        $kodbil->invoice_no = 'INV.SBJKT.'.$perm->nomor_permohonan;
        $kodbil->tarif_pnbp = 10000;

        $kodbil->app_id = self::APPS_ID;
        $kodbil->route_id = self::ROUTE_ID;
        $kodbil->user_id = self::USER_ID;
        $kodbil->user_id = self::PASSWORD;
        $kodbil->kode_kl = self::KODE_KL;
        $kodbil->kode_eselon_1 = self::KODE_ESELON_1;
        $kodbil->kode_satker = self::KODE_SATKER;
        $kodbil->jenis_pnbp = self::JENIS_PNBP; 
        $kodbil->kode_mata_uang = self::KODE_MATA_UANG;
        $header['trx_id'] = $kodbil->trx_id;
        $header['expired_date'] = $kodbil->expired_date;
        $header['create_date'] = $kodbil->date_sent;
        // $header['total_nominal_billing'] = 90;
        $header['total_nominal_billing'] = 10000;
        $header['nama_wajib_bayar'] = $kodbil->nama_wajib_bayar;
        $detail[] = [
            'NamaWajibBayar' => $kodbil->nama_wajib_bayar,
            // 'KodeTarifSimponi' => '001202',
            'KodeTarifSimponi' => '001202',
            'KodePPSimponi' => '201615C',
            // 'KodeAkun' => '423218',
            'KodeAkun' => '425515',
            // 'TarifPNBP' => 10000,
            'TarifPNBP' => $kodbil->tarif_pnbp,
            'Volume' => $kodbil->volume,
            'Satuan' => $kodbil->satuan,
            // 'TotalTarifPerRecord' => 90
            'TotalTarifPerRecord' => 10000
        ];
        $response = $this->PaymentRequest($kodbil->invoice_no, $header, $detail);
        $kodbil->br_simponi_trx_id = $response->simponiData->SimponiTrxId;
        $kodbil->kode_billing_simponi = $response->simponiData->KodeBillingSimponi;
        
        $kodbil->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Kode Billing Permohonan Anda Berhasil Didaftarkan. Silahkan Melakukan Pembayaran Sebelum Tanggal Expired pada <strong>". dateTimeToDate3($kodbil->expired_date). "</strong>. Kode Billing Anda adalah: <strong>" . $kodbil->kode_billing_simponi . "</strong>. Total yang harus dibayarkan adalah Rp " .($kodbil->volume*10000)
        ]);
        
        return redirect()->route('sk',compact('permohonans'));
        // return view('sk.index', compact('permohonans','simponi'));
        // return view('sk.invoice',compact('kodbil','perm'));
    }

    /* Response Payment Request*/
    // [response] => stdClass Object
    //     (
    //         [code] => 00
    //         [message] => Sukses
    //         [simponiData] => stdClass Object
    //             (
    //                 [SimponiTrxId] => 2019043005387481
    //                 [KodeBillingSimponi] => 820190430463976
    //                 [Date] => 2019-04-30 11:50:05
    //                 [ExpiredDate] => 2019-04-30 23:00:00
    //             )

    //     )

    /* Response Billing Status (Param Kodebilling dan invoicenum harus diisi) */
    // Array
    // (
    //     [ResponseCode] => NP
    //     [ResponseMessage] => Kode billing belum dibayar
    //     [ResponseData] => Array
    //         (
    //             [SimponiTrxId] => 2019043005387481
    //             [NTB] => 
    //             [NTPN] => 
    //             [TrxDate] => 2019-04-30 11:50:05
    //             [BankPersepsi] => 
    //             [ChannelPembayaran] => 
    //         )

    // )

    // Array
    // (
    //     [ResponseCode] => 00
    //     [ResponseMessage] => Sukses
    //     [ResponseData] => Array
    //         (
    //             [SimponiTrxId] => 201904281017726870
    //             [NTB] => 000000758175
    //             [NTPN] => 2663B3NGBAM6JCE9
    //             [TrxDate] => 2019-04-28 11:46:37
    //             [BankPersepsi] => 52000900099
    //             [ChannelPembayaran] => 7010
    //         )

    // )
}
