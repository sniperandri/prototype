<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permohonan;
use App\MasaLayar;
use DateTime;
use DateInterval;
use PDF;

class PrintController extends Controller
{
    public function pracetak(Request $request, $id){
        $interval = new DateTime();
    	$permohonan = Permohonan::findOrFail($id);
        $this->authorize('cetakSk',$permohonan);
    	$pelaut = $this->getPelaut($permohonan->seafarer_code);
        $masaLayar = MasaLayar::where('seafarer_code',$permohonan->seafarer_code)->get();
    	$masaLayar = $masaLayar->toArray();
        if(!empty($masaLayar)){
                $e = new DateTime();
                $f = clone $e;
            foreach($masaLayar as $masa){
                $e = $e->add(datediffinterval($masa['tgl_naik'],$masa['tgl_turun']));
            }
                $interval1 =  $f->diff($e);
        }
        
    	// $buku_pelaut = $this->getBukuPelaut('1201567968');
        $buku_pelaut = $this->getBukuPelaut($permohonan->seafarer_code);
            $h = new DateTime();
            $i = clone $h;
    	foreach($buku_pelaut as $vals){
    		$val = (Object) $vals;
    		$masaLayar[] = [
    			'nama_kapal'=>$val->NAMA_KAPAL,
    			'tonase_kotor'=>$val->TONASE_KOTOR,
    			'daya'=>$val->DAYA,
    			'daerah_layan'=>$val->DAERAH_PELAYARAN,
    			'jabatan'=>$val->JABATAN,
    			'tgl_naik'=>$val->TGL_NAIK,
    			'tgl_turun'=>$val->TGL_TURUN,
    		];
            $h = $h->add(datediffinterval($val->TGL_NAIK,$val->TGL_TURUN));
    	}
        
        $interval2 =  $i->diff($h);

        $theinterval = new DateTime();
        $thediff = clone $theinterval;
        $theinterval->add($interval1);
        $theinterval->add($interval2);
        $interval = $thediff->diff($theinterval)->format('%y tahun %m bulan %d hari');

        $tahun = explode(' ',$interval)[0];
        $bulan = explode(' ',$interval)[2];
        $hari = explode(' ',$interval)[4];

        return view('cetak.index',compact('permohonan','pelaut','masaLayar','buku_pelaut','interval','tahun','bulan','hari'));
    }

    public function qrcode(){
        return view('qr');
    }

    protected function url_get_contents ($Url) {
        if (!function_exists('curl_init')){ 
            die('CURL is not installed!');
        }
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    protected function getPelaut($code){
        ini_set('default_socket_timeout', 15);
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
    	header('Access-Control-Allow-Origin: *');
    	$url = "https://pelaut.dephub.go.id/webapp/finddata/1/{$code}";
        // $response = $this->url_get_contents($url);
        $response = @file_get_contents($url, true);
            if($response){
                $data = json_decode($response,true);
                return $data;
            }else{
                return redirect()->route('network');
            }
    }


    protected function getBukuPelaut($code){
        ini_set('default_socket_timeout', 15);
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        header('Access-Control-Allow-Origin: *');
        $url = "https://dokumenpelaut.dephub.go.id/ws/sijil?sc={$code}";
        $response = @file_get_contents($url, false, stream_context_create($arrContextOptions));
        if($response){
            $data = json_decode($response,true);
            return $data;
        }else{
            die('<strong>Website http://dokumenpelaut.dephub.go.id untuk mengambil data tidak bisa diakses, silahkan coba beberapa saat lagi. </strong>');
        }
    }

    // public function downloadPDF($id){   
    //     $permohonan = Permohonan::findOrFail($id);
    //     $bukuPelaut = $this->getBukuPelaut($permohonan->seafarer_code);
    //     $pelaut = $this->getPelaut($permohonan->seafarer_code);  

    //     $pdf = PDF::loadView('pdf', compact('permohonan','pelaut'));
    //           return $pdf->stream('sk-masa-layar.pdf');
    // }

    public function downloadPDF($id){
        $interval = new DateTime();
        $permohonan = Permohonan::findOrFail($id);

        $latestNumber = Permohonan::whereNotNull('nomor_sertifikat')->orderBy('id','desc')->first();

        if(!$latestNumber){
            $latestNumber = 'No.AL.506/0000/I/Syb.Tpk-19';
        }else{
            $latestNumber = $latestNumber->nomor_sertifikat;
        }

        $permohonan->nomor_sertifikat = autonumberSertifikat($latestNumber);

        $this->authorize('cetakSk',$permohonan);
        $masaLayar = MasaLayar::where('seafarer_code',$permohonan->seafarer_code)->get();
        $masaLayar = $masaLayar->toArray();
        if(!empty($masaLayar)){
                $e = new DateTime();
                $f = clone $e;
            foreach($masaLayar as $masa){
                $e = $e->add(datediffinterval($masa['tgl_naik'],$masa['tgl_turun']));
            }
                $interval1 =  $f->diff($e);
        }
        
        $pelaut = $this->getPelaut($permohonan->seafarer_code);
        // $buku_pelaut = $this->getBukuPelaut('1201567968');
        $buku_pelaut = $this->getBukuPelaut($permohonan->seafarer_code);
            $h = new DateTime();
            $i = clone $h;
        foreach($buku_pelaut as $vals){
            $val = (Object) $vals;
            $masaLayar[] = [
                'nama_kapal'=>$val->NAMA_KAPAL,
                'tonase_kotor'=>$val->TONASE_KOTOR,
                'daya'=>$val->DAYA,
                'daerah_layan'=>$val->DAERAH_PELAYARAN,
                'jabatan'=>$val->JABATAN,
                'tgl_naik'=>$val->TGL_NAIK,
                'tgl_turun'=>$val->TGL_TURUN,
            ];
            $h = $h->add(datediffinterval($val->TGL_NAIK,$val->TGL_TURUN));
        }
        
        $interval2 =  $i->diff($h);

        $theinterval = new DateTime();
        $thediff = clone $theinterval;
        $theinterval->add($interval1);
        $theinterval->add($interval2);
        $interval = $thediff->diff($theinterval)->format('%y tahun %m bulan %d hari');

        $tahun = explode(' ',$interval)[0];
        $bulan = explode(' ',$interval)[2];
        $hari = explode(' ',$interval)[4];

        $pdf = PDF::loadView('pdf', compact('permohonan','pelaut','masaLayar','buku_pelaut','interval','tahun','bulan','hari'));
                      return $pdf->stream('sk-masa-layar.pdf');

        // return view('cetak.index',compact('permohonan','pelaut','masaLayar','buku_pelaut','interval','tahun','bulan','hari'));
    }

    public function bukti(Request $request, $permohonan){
        $permohonan = Permohonan::findOrFail($permohonan);
        $this->authorize('cetakBukti',$permohonan);
        $pelaut = $this->getPelaut($permohonan->seafarer_code);  

        $pdf = PDF::loadView('bukti-permohonan', compact('pelaut','permohonan'));
        return $pdf->stream('bukti-permohonan.pdf');

        // $pdf = PDF::loadView('bukti-coba', compact('permohonan'))
        //         ->setPaper('a4');
        // return $pdf->stream('bukti-coba.pdf');
    }

    public function network(){
        return view('auth/network');
    }
}
