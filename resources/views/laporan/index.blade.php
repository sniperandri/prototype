@extends('layouts.bracket.main')

@section('style')
<style>
    .form-layout-1 {
        border: 1px solid #ced4da;
        padding-top: 30px;
        padding-right: 30px;
        padding-bottom: 0px;
        padding-left: 30px;
        margin-bottom: 0px;
    }
</style>
@endsection

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">LAPORAN MASA BERLAYAR</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA</p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        @include('layouts._flash')
        <div class="form-layout form-layout-1">
        <h5>PERIODE LAPORAN YANG SUDAH DIPROSES</h5>
            <form action="/filter" method="get">
              <div class="row">
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Dari Tanggal: </label>
                    {!! Form::date('dari', '',['class'=>'form-control']); !!}
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Sampai Dengan Tanggal: </label>
                    {!! Form::date('sampai', '',['class'=>'form-control']); !!}
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label"><span class="tx-danger" style="color:white">*</span></label>
                    <button class="btn btn-info" style="cursor:pointer;">Filter</button>
                  </div>
                </div><!-- col-4 -->
              </div><!-- row -->
            </form>
        </div><!-- form-layout -->
        <br>
        <div class="br-section-wrapper">
            @include('layouts._flash')
            <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Daftar Permohonan Yang Sudah Diproses</h6>
            <div class="d-flex">
                <div class="wd-50p wd-md-50p wd-lg-50p">
                </div>
            </div>
            <div class="bd bd-gray-300 rounded table-responsive">
                <table class="table table-striped mg-b-0">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nomor Permohonan</th>
                            <th>Seafarer Code</th>
                            <th>Pemohon</th>
                            <th>Keperluan</th>
                            <th class="text-center" width="30%">Status</th>
                            <th class="text-center">Pembayaran PNBP</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;?>
                        @foreach($permohonans as $permohonan)
                        <tr>
                            <th scope="row">{{ $no }}</th>
                            <td>{{ $permohonan->nomor_permohonan }}</td>
                            <td>{{ $permohonan->seafarer_code }}</td>
                            <td>{{ $permohonan->user ? $permohonan->user->name : ''}}</td>
                            <td>{{ $permohonan->keperluan }}</td>
                            <td align="center">
                                @if($permohonan->status > 1)
                                    <h6><p class="badge badge-success"><i class="fa fa-check-circle"></i> Approved</p></h6>
                                    Diproses Oleh : {{ $permohonan->approved ? $permohonan->approved->name : '' }} <br>
                                    Pada : {{ $permohonan->approved_time ? $permohonan->approved_time->diffForHumans() : '' }}
                                @elseif($permohonan->status == 1)
                                    <h6><p class="badge badge-primary"><i class="fa fa-refresh"></i> Sedang Diproses</p></h6>
                                @else
                                    <h6><p class="badge badge-danger"><i class="fa fa-times-circle"></i> Permohonan Ditolak</p></h6>
                                @endif
                            </td>
                            <td align="center">
                                @if($permohonan->status > 3)
                                <p class="badge badge-success"><i class="fa fa-check-circle"></i> Sudah Bayar</p>
                                @elseif($permohonan->status == 0)
                                <h6><p class="badge badge-danger"><i class="fa fa-times-circle"></i> Permohonan Ditolak</p></h6>
                                @else
                                <p class="badge badge-danger"><i class="fa fa-times-circle"></i> Belum Bayar</p>
                                @endif
                            </td>
                            <td align="center">
                                @if($permohonan->status == 1)
                                <h5><a href="{{ route('permohonan.proses',$permohonan->id) }}" class="btn btn-primary btn-block btn-sm"><i class="fa fa-refresh"></i> Proses</a></h5>
                                @elseif($permohonan->status > 1)
                                <h5><a href="{{ route('permohonan.masa-layar',$permohonan->id) }}" class="btn btn-info btn-block btn-sm"><i class="fa fa-search"></i> Lihat Masa Layar</a></h5>
                                <h5><a href="{{ route('print-pdf', $permohonan->id) }}" target="_blank" class="btn btn-warning btn-sm btn-block"><i class="fa fa-file-pdf-o"></i> Lihat SK Masa Layar</a></h5>
                                @else
                                -
                                @endif
                            </td>
                        <?php $no++;?>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- br-section-wrapper -->
        <br>
        <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Grafik Jumlah Permohonan Masa Layar</h6>
        <div class="d-flex">
            <div class="wd-50p wd-md-50p wd-lg-50p">
            </div>
        </div>
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->

@include('layouts.bracket.footer')
</div>
@endsection
@section('script')
<script>
	fetch('http://localhost:8000/api/laporan-permohonan')
        .then((response) => {
            return response.json();
        }).then((permohonan) => {
        	console.log(permohonan['approved']);
        	Highcharts.theme = {
        	    colors: ['#058DC7', '#50B432', '#F90F0F', '#DDDF00', '#24CBE5', '#64E572', 
        	             '#FF9655', '#FFF263', '#6AF9C4'],
        	    chart: {
        	        backgroundColor: {
        	            linearGradient: [0, 0, 500, 500],
        	            stops: [
        	                [0, 'rgb(255, 255, 255)'],
        	                [1, 'rgb(123, 130, 255)']
        	            ]
        	        },
        	    },
        	    title: {
        	        style: {
        	            color: '#000',
        	            font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
        	        }
        	    },
        	    subtitle: {
        	        style: {
        	            color: '#666666',
        	            font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
        	        }
        	    },

        	    legend: {
        	        itemStyle: {
        	            font: '9pt Trebuchet MS, Verdana, sans-serif',
        	            color: 'black'
        	        },
        	        itemHoverStyle:{
        	            color: 'gray'
        	        }   
        	    }
        	};

        	// Apply the theme
        	Highcharts.setOptions(Highcharts.theme);
        	Highcharts.chart('container', {
        	    chart: {
        	        type: 'column'
        	    },
        	    title: {
        	        text: 'Jumlah Permohonan Masa Layar'
        	    },
        	    xAxis: {
        	        categories: permohonan['month']
        	    },
        	    yAxis: {
        	        min: 0,
        	        title: {
        	            text: 'Jumlah Permohonan'
        	        },
        	        stackLabels: {
        	            enabled: true,
        	            style: {
        	                fontWeight: 'bold',
        	                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
        	            }
        	        }
        	    },
        	    legend: {
        	        align: 'right',
        	        x: -30,
        	        verticalAlign: 'top',
        	        y: 25,
        	        floating: true,
        	        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        	        borderColor: '#CCC',
        	        borderWidth: 1,
        	        shadow: false
        	    },
        	    tooltip: {
        	        headerFormat: '<b>{point.x}</b><br/>',
        	        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        	    },
        	    plotOptions: {
        	        column: {
        	            stacking: 'normal',
        	            dataLabels: {
        	                enabled: true,
        	                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
        	            }
        	        }
        	    },
        	    series: [{
        	        name: 'Dalam Proses',
        	        data: permohonan['proses']
        	    }, {
        	        name: 'Sudah Diproses',
        	        data: permohonan['approved']
        	    }]
        	});
        })
</script>
@endsection