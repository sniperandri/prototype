@extends('layouts.bracket.main')

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">VERIFIKASI SK MASA BERLAYAR</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA</p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        @include('layouts._flash')
        <div class="row">
            <div class="col-md-8">
                <h2 class="tx-gray-800 tx-uppercase tx-bold tx-28 mg-b-10">Daftar Permohonan</h2>
            </div>
            <div class="col-md-4 text-right">
                <form action="/cari-permohonan" method="get">
                    <div class="input-group">
                        <input type="search" class="form-control" name="search" placeholder="Cari Nomor Permohonan">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit" style="cursor: pointer;">Cari</button>
                        </span>
                    </div><!-- /input-group -->
                </form>
            </div>
        </div>
        <br>
        <div class="d-flex">
            <div class="wd-50p wd-md-50p wd-lg-50p">
            </div>
        </div>
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                    <tr>
                        <th class="text-center">No.</th>
                        <th>Nomor Permohonan</th>
                        <th>Seafarer Code</th>
                        <th>Pemohon</th>
                        <th>Keperluan</th>
                        <th class="text-center" width="30%">Status</th>
                        <th class="text-center">Pembayaran PNBP</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($permohonans as $key => $permohonan)
                    <tr>
                        <td align="center" scope="row">{{ $key + $permohonans->firstItem() }}</td align="center">
                        <td>
                            {{ $permohonan->nomor_permohonan }}
                            <br>
                            <p style="font-size: 12px">
                                Tanggal : {{ $permohonan->created_at ? dateTimeToDate3($permohonan->created_at) : '' }}
                            </p>
                        </td>
                        <td>{{ $permohonan->seafarer_code }}</td>
                        <td>{{ $permohonan->user->name }}</td>
                        <td>{{ $permohonan->keperluan }}</td>
                        <td align="center">
                            @if($permohonan->status > 1)
                                <h6><p class="badge badge-success"><i class="fa fa-check-circle"></i> Approved</p></h6>
                                Diproses Oleh : {{ $permohonan->approved ? $permohonan->approved->name : '' }} <br>
                                Pada : {{ $permohonan->approved_time ? $permohonan->approved_time->diffForHumans() : '' }}
                            @elseif($permohonan->status == 1)
                                <h6><p class="badge badge-primary"><i class="fa fa-refresh"></i> Sedang Diproses</p></h6>
                            @else
                                <h6><p class="badge badge-danger"><i class="fa fa-times-circle"></i> Permohonan Ditolak</p></h6>
                            @endif
                        </td>
                        <td align="center">
                            @if($permohonan->status > 3)
                            <p class="badge badge-success"><i class="fa fa-check-circle"></i> Sudah Bayar</p>
                            @elseif($permohonan->status == 0)
                            <h6><p class="badge badge-danger"><i class="fa fa-times-circle"></i> Permohonan Ditolak</p></h6>
                            @else
                            <p class="badge badge-danger"><i class="fa fa-times-circle"></i> Belum Bayar</p>
                            @endif
                        </td>
                        <td align="center">
                        	@if($permohonan->status == 1)
                        	<h5><a href="{{ route('permohonan.proses',$permohonan->id) }}" class="btn btn-primary btn-block btn-sm"><i class="fa fa-refresh"></i> Proses</a></h5>
                        	@elseif($permohonan->status > 1)
                            <h5><a href="{{ route('permohonan.masa-layar',$permohonan->id) }}" class="btn btn-info btn-block btn-sm"><i class="fa fa-search"></i> Lihat Masa Layar</a></h5>
                            <h5><a href="{{ route('print-pdf', $permohonan->id) }}" target="_blank" class="btn btn-warning btn-sm btn-block"><i class="fa fa-file-pdf-o"></i> Lihat SK Masa Layar</a></h5>
                            @else
                        	-
                        	@endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div>
            Halaman : {{ $permohonans->currentPage() }} <br/>
            Total Data : {{ $permohonans->total() }} <br/>
            Data Per Halaman : {{ $permohonans->perPage() }} <br/>
            {{ $permohonans->links() }}
        </div>
    </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('layouts.bracket.footer')
</div>
@endsection
