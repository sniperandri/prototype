@extends('layouts.bracket.main')

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">VERIFIKASI SK MASA BERLAYAR</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA</p>
</div>

<div class="br-pagebody">
<div class="br-section-wrapper">
  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Informasi Pelaut</h6>

  <div class="form-layout form-layout-1">
    <div class="row mg-b-25">
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Nomor Permohonan: </label>
          <input class="form-control" type="text" name="firstname" placeholder="{{ $permohonan['nomor_permohonan'] }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
      <div class="col-lg-8">
        <div class="form-group">
          <label class="form-control-label">Keperluan: </label>
          <input class="form-control" type="text" name="lastname" placeholder="{{ $permohonan['keperluan'] }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Nama: </label>
          <input class="form-control" type="text" name="firstname" placeholder="{{ $pelaut['nama'] }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Tempat Lahir: </label>
          <input class="form-control" type="text" name="lastname" placeholder="{{ $pelaut['tempat_lahir'] }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Tanggal Lahir: </label>
          <input class="form-control" type="text" name="email" placeholder="{{ dateTimeToDate($pelaut['tanggal_lahir']) }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
    </div><!-- row -->

    <div class="bd bd-gray-300 rounded table-responsive">
        <table class="table table-striped mg-b-0">
            <thead>
                <tr>
                    <th>Nama Sertifikat</th>
                    <th>Jenis Sertifikat</th>
                    <th class="text-center">Nomor Sertifikat</th>
                    <th class="text-center">Kode Blanko</th>
                    <th class="text-center">Tanggal Berlaku</th>
                    <th class="text-center">Tanggal Berakhir</th>
                </tr>
            </thead>
            <tbody>
              @foreach($pelaut['sertifikat'] as $sertifikat)
                <tr>
                    <td>{{ $sertifikat['nama'] }}</td>
                    <td>{{ $sertifikat['jenis'] }}</td>
                    <td align="center">{{ $sertifikat['no_sertifikat'] }}</td>
                    <td align="center">{{ $sertifikat['kode_blanko'] }}</td>
                    <td align="center">{{ $sertifikat['tgl_mulai'] }}</td>
                    <td align="center">{{ $sertifikat['tgl_akhir'] }}</td>
                </tr>
              @endforeach
            </tbody>
        </table>
    </div>

    <br>

    <div class="form-layout-footer">
        <a href="{{ route('permohonan.approve',$permohonan->id) }}" class="btn btn-success active"><i class="fa fa-check"></i> Verifikasi dan Input Masa Berlayar</a>
        <a href="{{ route('permohonan.reject',$permohonan->id) }}" class="btn btn-danger active"><i class="fa fa-times"></i> Tolak Permohonan</a>
    </div><!-- form-layout-footer -->
  </div><!-- form-layout -->


</div>
@include('layouts.bracket.footer')
@endsection