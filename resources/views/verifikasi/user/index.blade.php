@extends('layouts.bracket.main')
@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">VERIFIKASI USER</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA</p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        @include('layouts._flash')
        <div class="row">
            <div class="col-md-8">
                <h2 class="tx-gray-800 tx-uppercase tx-bold tx-28 mg-b-10">Daftar User</h2>
            </div>
            <div class="col-md-4 text-right">
                <form action="/cari-user" method="get">
                    <div class="input-group">
                        <input type="search" class="form-control" name="search" placeholder="Cari User">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit" style="cursor: pointer;">Cari</button>
                        </span>
                    </div><!-- /input-group -->
                </form>
            </div>
        </div>
        <br>
        <div class="d-flex">
            <div class="wd-50p wd-md-50p wd-lg-50p">
            </div>
        </div>
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th class="text-center">Status Email</th>
                        <th class="text-center">Status User</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($registrar as $key => $register)
                    <tr>
                        <th scope="row">{{ $key + $registrar->firstItem() }}</th>
                        <td>{{ $register->name }}</td>
                        <td>{{ $register->email }}</td>
                        <td align="center">
                            @if($register->status_email == 2)
                                <h5><span class="badge badge-success" style="color:white;"><i class="fa fa-check"></i>  User Sudah Verifikasi</span></h5>
                            @elseif($register->status_email == 1)
                                <h5><span class="badge badge-primary" style="color:white;"><i class="fa fa-refresh"></i>  User Belum Verifikasi</span></h5>
                            @else
                                <h5><span class="badge badge-danger" style="color:white;"><i class="fa fa-times"></i>  Permohonan Ditolak</span></h5>
                            @endif
                        </td>
                        <td align="center">
                            @if($register->status_user == 2)
                                <h5><span class="badge badge-success" style="color:white;"><i class="fa fa-check"></i>  Aktif</span></h5>
                                Diproses Oleh : {{ $register->approved($register->approved_by)}} <br>
                                Pada : {{ $register->when($register->id)}} <br>
                            @else
                                <h5><span class="badge badge-primary" style="color:white;"><i class="fa fa-refresh"></i>  Sedang Diproses</span></h5>
                            @endif
                        </td>
                        @if($register->status_user == 1)
                        <td align="center">
                        	<h5><a title="Approve" href="{{ route('user.approve',$register->id) }}" class="badge badge-success"><i class="fa fa-check"></i> </a> | 
                            <a title="Tolak" href="{{ route('user.reject',$register->id) }}" class="badge badge-danger"><i class="fa fa-times"></i> </a></h5>
                        </td>
                        @else
                        <td align="center">
                        	-
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div>
            Halaman : {{ $registrar->currentPage() }} <br/>
            Total Data : {{ $registrar->total() }} <br/>
            Data Per Halaman : {{ $registrar->perPage() }} <br/>
            {{ $registrar->links() }}
        </div>
    </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('layouts.bracket.footer')
</div>
@endsection