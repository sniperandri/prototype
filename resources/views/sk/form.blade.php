@extends('layouts.bracket.main')

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">SK MASA BERLAYAR</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA</p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Form Permohonan</h6>

        <div class="form-layout form-layout-1">
    <div class="row mg-b-25">
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Nama: </label>
          <input class="form-control" type="text" name="firstname" placeholder="{{ $pemohon->name }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Seafarer Code: </label>
          <input class="form-control" type="text" name="lastname" placeholder="{{ $pemohon->seafarer_code }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Tanggal Lahir: </label>
          <input class="form-control" type="text" name="email" placeholder="{{ dateTimeToDate($pemohon->tanggal_lahir) }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->

        <table class="table table-striped mg-b-0">
            <thead>
                <tr>
                    <th>Nama Sertifikat</th>
                    <th>Jenis Sertifikat</th>
                    <th class="text-center">Nomor Sertifikat</th>
                    <th class="text-center">Kode Blanko</th>
                    <th class="text-center">Tanggal Berlaku</th>
                    <th class="text-center">Tanggal Berakhir</th>
                </tr>
            </thead>
            <tbody>
              @foreach($pelaut['sertifikat'] as $sertifikat)
                <tr>
                    <td>{{ $sertifikat['nama'] }}</td>
                    <td>{{ $sertifikat['jenis'] }}</td>
                    <td align="center">{{ $sertifikat['no_sertifikat'] }}</td>
                    <td align="center">{{ $sertifikat['kode_blanko'] }}</td>
                    <td align="center">{{ $sertifikat['tgl_mulai'] }}</td>
                    <td align="center">{{ $sertifikat['tgl_akhir'] }}</td>
                </tr>
              @endforeach
            </tbody>
        </table>
    </div>
    <br>
    <div class="form-layout form-layout-1">
    {!! Form::open(['route' => 'submit-permohonan','method'=>'post','files'=>TRUE]) !!}
        <div class="row mg-b-25">
            <div class="col-lg-6">
                <div class="form-group mg-b-10-force {{ $errors->has('keperluan') ? 'has-error' : ''}}">
                    {!! Form::label('keperluan', 'Keperluan :'); !!}
                    {!! Form::text('keperluan', '',['class'=>'form-control','placeholder'=>'Masukkan Keperluan Anda, Contoh: (Diklat,dll)']); !!}
                    @if($errors->has('keperluan'))
                        <span class="badge badge-danger">{{ $errors->first('keperluan') }}</span>
                    @endif
                </div>
            </div><!-- col-6 -->
            <div class="col-lg-6">
                <div class="form-group mg-b-10-force">
                    {!! Form::label('file_upload', 'Upload Buku Pelaut :'); !!}<br>
                    {!! Form::file('file_upload'); !!}
                    @if($errors->has('file_upload'))
                    <span class="badge badge-danger">{{ $errors->first('file_upload') }}</span>
                @endif
                </div>
            </div><!-- col-6 -->
        </div><!-- row -->
        {!! Form::submit('Submit Permohonan',['class'=>'btn btn-info', 'id'=>'tombol']) !!}
    {!! Form::close() !!}
    </div><!-- form-layout -->
  </div><!-- form-layout -->

        
    </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('layouts.bracket.footer')
</div>
@endsection