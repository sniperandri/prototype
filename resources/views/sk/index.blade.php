@extends('layouts.bracket.main')

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">CETAK KARTU</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA.</p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        @include('layouts._flash')
        <div class="row">
            <div class="col-md-8">
                <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Cetak Kartu - {{ Auth::user()->name }}</h6>
            </div>
            <div class="col-md-4">
                <form action="/cari-permohonan" method="get">
                    <div class="input-group">
                        <input type="search" class="form-control" name="search" placeholder="Cari Kartu">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit" style="cursor: pointer;">Cari</button>
                        </span>
                    </div><!-- /input-group -->
                </form>
            </div>
        </div>

        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                    <tr>
                        <th class="text-center">No.</th>
                        <th class="text-center">Nama Anggota</th>
                        <th class="text-center">Nomor Anggota</th>
                        <th class="text-center">Member Since</th>
                        <th class="text-center">Preview Kartu</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="center">1</td>
                        <td align="center">Rahmat Hidayat</td>
                        <td align="center">123 456 789</td>
                        <td align="center">02 / 12</td>
                        <td align="center">
                            <img src="{{ asset('/img/kartumiles.jpeg') }}" width="250px;" style="margin-bottom: 10px;" /><br>
                            <h5><a title="Cetak Kartu" href="#" class="badge badge-dark"><i class="fa fa-print"></i> Cetak Kartu</a></h5>
                        </td>
                    </tr>
                </tbody>
            </table>
            
        </div>
    </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('layouts.bracket.footer')
</div>
@section('script')
<script>
    $("#cetak").on('click',function(){
        $("#cetak").hide();
    });
</script>
@endsection