<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registrasi Pendaftaran HEPI</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div>
        Hi, {{ $user->name }}
        Permohonan akun Anda sudah kami terima, klik link di bawah ini untuk verifikasi email Anda: <br />
		<a href="{{ route('verReg',['id'=>$user->id])}}">Verifikasi Email</a>
        <br />
        Jika link tidak berfungsi, copy dan paste link berikut ke browser Anda : <br />
        {{ route('verReg',['id'=>$user->id])}}
    </div>	
</body>
</html>