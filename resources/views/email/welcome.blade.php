@extends('beautymail::templates.ark')

@section('content')

    @include('beautymail::templates.ark.heading', [
		'heading' => 'Registrasi Akun Himpunan Evaluasi Pendidikan Indonesia',
		'level' => 'h1'
	])

    @include('beautymail::templates.ark.contentStart')

        <h4 class="secondary"><strong>Hi, {{ $user->name }}</strong></h4>
        Permohonan akun Anda sudah kami terima, klik link di bawah ini untuk verifikasi email Anda: <br />
		<a href="{{ route('verReg',['id'=>$user->id])}}">Verifikasi Email</a>
        <br />
        Jika link tidak berfungsi, copy dan paste link berikut ke browser Anda : <br />
        {{ route('verReg',['id'=>$user->id])}}

        @include('beautymail::templates.minty.button', ['text' => 'Verifikasi', 'link' => {{ route('verReg',['id'=>$user->id])}}])

    @include('beautymail::templates.ark.contentEnd')

@stop