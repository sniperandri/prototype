<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Verifikasi Pendaftaran Akun HEPI</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div>
        Hi, {{ $user->name }}
        Permohonan akun Anda sudah disetujui oleh Petugas, silahkan kembali ke aplikasi dan melakukan login.

        Terima kasih.
        Admin Himpunan Evaluasi Pendidikan Indonesia.
    </div>	
</body>
</html>