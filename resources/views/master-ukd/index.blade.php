@extends('layouts.bracket.main')

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">MASTER DATA UKD</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA</p>
</div>

<div class="br-pagebody">
<div class="br-section-wrapper">
        @include('layouts._flash')
        <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">MASTER UKD</h6>
        <div class="d-flex">
            <div class="wd-50p wd-md-50p wd-lg-50p">
            </div>
            <div class="wd-50p wd-md-50p wd-lg-50p">
                <a href="{{ route('master-ukd.create') }}" class="btn btn-primary bd-1 tx-uppercase tx-bold tx-10 mg-b-10 pull-right"><i class="fa fa-plus-circle"></i> Tambah Master UKD</a>
            </div>
        </div>
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama UKD</th>
                        <th>Ketua UKD</th>
                        <th>Alamat UKD</th>
                        <th>Telepon UKD</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;?>
                    @foreach($masterUkd as $ukd)
                    <tr>
                        <th scope="row">{{ $no }}</th>
                        <td>{{ $ukd->nama_ukd }}</td>
                        <td>{{ $ukd->ketua_ukd }}</td>
                        <td>{{ $ukd->alamat_ukd }}</td>
                        <td>{{ $ukd->telepon_ukd }}</td>

                        <td align="center">
                          {!! Form::open(['method' => 'DELETE', 'route' => ['master-ukd.destroy', $ukd->id]]) !!}
                          <a href="{{ route('master-ukd.edit', $ukd->id) }}" class="btn btn-sm btn-warning btn-block" title="Edit">
                            <i class="fa fa-edit text-black"></i> Edit
                          </a>
                            <button onclick="return confirm('Apakah Anda yakin untuk menghapus petugas?')" type="submit" class="btn btn-sm btn-danger btn-block" title="Move to Trash">
                              <i class="fa fa-trash"></i> Hapus
                            </button>
                          {!! Form::close() !!}
                        </td>
                    <?php $no++;?>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div><!-- br-section-wrapper -->
</div>
@include('layouts.bracket.footer')
@endsection