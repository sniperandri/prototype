@extends('layouts.bracket.main')
@section('style')
<style>
    .br-footer {
        position: fixed;
        left: 1000;
        bottom: 0;
        width: 100%;
    }
    #tombol{
        cursor: pointer;
    }
</style>
@endsection

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">TAMBAH MASTER UKD</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA.</p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Input Master UKD</h6>

        <div class="form-layout form-layout-1">
        {!! Form::open(['route' => 'submit-master-ukd','method'=>'post','files'=>TRUE]) !!}
        @csrf
            <div class="row mg-b-25">
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('nama_ukd') ? 'has-error' : ''}}">
                        {!! Form::label('nama_ukd', 'Nama Master UKD :'); !!}
                        {!! Form::text('nama_ukd', '',['class'=>'form-control','placeholder'=>'Input Nama Master UKD']); !!}
                        @if($errors->has('nama_ukd'))
                            <span class="badge badge-danger">{{ $errors->first('nama_ukd') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('ketua_ukd') ? 'has-error' : ''}}">
                        {!! Form::label('ketua_ukd', 'Nama Ketua UKD :'); !!}
                        {!! Form::text('ketua_ukd', '',['class'=>'form-control','placeholder'=>'Input Nama Ketua UKD']); !!}
                        @if($errors->has('ketua_ukd'))
                            <span class="badge badge-danger">{{ $errors->first('ketua_ukd') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('telepon_ukd') ? 'has-error' : ''}}">
                        {!! Form::label('telepon_ukd', 'Nomor Telepon UKD :'); !!}
                        {!! Form::text('telepon_ukd', '',['class'=>'form-control','placeholder'=>'Input Nomor Telepon UKD']); !!}
                        @if($errors->has('telepon_ukd'))
                            <span class="badge badge-danger">{{ $errors->first('telepon_ukd') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-12">
                    <div class="form-group mg-b-10-force {{ $errors->has('alamat_ukd') ? 'has-error' : ''}}">
                        {!! Form::label('alamat_ukd', 'Alamat UKD :'); !!}
                        {!! Form::textarea('alamat_ukd', '',['class'=>'form-control','placeholder'=>'Input Alamat UKD', 'rows'=>6]); !!}
                        @if($errors->has('alamat_ukd'))
                            <span class="badge badge-danger">{{ $errors->first('alamat_ukd') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
            </div><!-- row -->
            {!! Form::submit('Tambahkan Master UKD',['class'=>'btn btn-info', 'id'=>'tombol']) !!}
        {!! Form::close() !!}
        </div><!-- form-layout -->
    </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('layouts.bracket.footer')
</div>
@endsection