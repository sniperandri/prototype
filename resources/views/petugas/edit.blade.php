@extends('layouts.bracket.main')
@section('style')
<style>
    #tombol{
        cursor: pointer;
    }
</style>
@endsection

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">EDIT PETUGAS</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA.</p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Input Petugas</h6>
        <div class="form-layout form-layout-1">
        {!! Form::model($petugas, [
            'method' => 'PUT',
            'route' => ['petugas.update', $petugas->id],
            'id' => 'petugas-form',
        ]) !!}
        @csrf
            <div class="row mg-b-25">
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('name') ? 'has-error' : ''}}">
                        {!! Form::label('name', 'Nama Petugas :'); !!}
                        {!! Form::text('name', $petugas->name,['class'=>'form-control']); !!}
                        @if($errors->has('name'))
                            <span class="badge badge-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('email') ? 'has-error' : ''}}">
                        {!! Form::label('email', 'Email :'); !!}
                        {!! Form::text('email', $petugas->email,['class'=>'form-control','placeholder'=>$petugas->email ]); !!}
                        @if($errors->has('email'))
                            <span class="badge badge-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('password') ? 'has-error' : ''}}">
                        {!! Form::label('password', 'Password') !!}
                        {!! Form::text('password',$petugas->password, ['class'=>'form-control','placeholder' => $petugas->password]) !!}
                        @if($errors->has('password'))
                            <span class="badge badge-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('jenis_kelamin') ? 'has-error' : ''}}">
                        {!! Form::label('jenis_kelamin', 'Jenis Kelamin') !!}
                        {!! Form::select('jenis_kelamin', ['1' => 'Laki-Laki', '2' => 'Perempuan'], null, ['class'=>'form-control select','placeholder' => 'Pilih Jenis Kelamin']) !!}
                        @if($errors->has('jenis_kelamin'))
                            <span class="badge badge-danger">{{ $errors->first('jenis_kelamin') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('no_hp') ? 'has-error' : ''}}">
                        {!! Form::label('no_hp', 'Nomor HP :'); !!}
                        {!! Form::text('no_hp', $petugas->no_hp,['class'=>'form-control']); !!}
                        @if($errors->has('no_hp'))
                            <span class="badge badge-danger">{{ $errors->first('no_hp') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('nik') ? 'has-error' : ''}}">
                        {!! Form::label('nik', 'NIK :'); !!}
                        {!! Form::text('nik', $petugas->nik,['class'=>'form-control']); !!}
                        @if($errors->has('nik'))
                            <span class="badge badge-danger">{{ $errors->first('nik') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('role') ? 'has-error' : ''}}">
                        {!! Form::label('role', 'Jabatan') !!}
                        {!! Form::select('role', ['KK' => 'Kepala Kantor', 'KABID' => 'KABID', 'KASI' => 'KASI', 'STAFF' => 'STAFF'], null, ['class'=>'form-control select','placeholder' => 'Pilih Jabatan']) !!}
                        @if($errors->has('role'))
                            <span class="badge badge-danger">{{ $errors->first('role') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
            </div><!-- row -->
            {!! Form::submit('Edit Petugas',['class'=>'btn btn-info', 'id'=>'tombol']) !!}
        {!! Form::close() !!}
        </div><!-- form-layout -->
    </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
@include('layouts.bracket.footer')
</div>
@endsection