@extends('layouts.bracket.main')

@section('style')
<style>
	.br-footer {
		position: fixed;
	  	left: 1000;
	  	bottom: 0;
	  	width: 100%;
	}
</style>
@endsection
@section('content')
	<!-- ########## START: MAIN PANEL ########## -->
	<div class="br-mainpanel content">
	  <div class="pd-30">
	    <h4 class="tx-gray-800 mg-b-5">Dashboard</h4>
	    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA</p>
	  </div><!-- d-flex -->

	  @include('layouts.bracket.footer')
	</div><!-- br-mainpanel -->
	<!-- ########## END: MAIN PANEL ########## -->
@endsection
