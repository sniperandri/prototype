<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Himpunan Evaluasi Pendidikan Indonesia - Enhancing Quality Education Through Quality Evaluation">
    <meta name="author" content="ThemePixels">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
    <title>{{ config('app.name', 'HIMPUNAN EVALUASI PENDIDIKAN INDONESIA') }}</title>

    <!-- vendor css -->
    <link href="{{ asset('bracket/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('bracket/ionicons/css/ionicons.css') }}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{ asset('bracket/css/bracket.css') }}">
  </head>

  <body>

    @yield('content')

    <script src="{{ asset('bracket/jquery/jquery.js') }}"></script>
    <script src="{{ asset('bracket/popper.js/popper.js') }}"></script>
    <script src="{{ asset('bracket/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ asset('bracket/select2/js/select2.min.js') }}"></script>

    @yield('script')
  </body>
</html>
