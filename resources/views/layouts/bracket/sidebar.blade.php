<!-- ########## START: LEFT PANEL ########## -->
<div class="br-logo"><a href=""><span>[</span>bracket<span>]</span></a></div>
<div class="br-logo tx-20"><a href="{{ url('/') }}"><img src="{{ asset('img/logohepi.png') }}" width="42"> &nbsp; HEPI </a></div>
<div class="br-sideleft overflow-y-auto">
  <label class="sidebar-label pd-x-15 mg-t-20">Navigation</label>
  <div class="br-sideleft-menu">
    <a href="{{ url('/') }}" class="br-menu-link active">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
        <span class="menu-item-label">Dashboard</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    @can('isUser')
    <label class="sidebar-label pd-x-15 mg-t-20">Modul Pelayanan</label>
    <a href="{{ route('sk') }}" class="br-menu-link">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-ios-email-outline tx-24"></i>
        <span class="menu-item-label">CETAK KARTU</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    @endcan
    @can('isPetugas')
    <label class="sidebar-label pd-x-15 mg-t-20">Modul Verifikasi</label>
    <a href="{{ route('verifikasi-user') }}" class="br-menu-link">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-person-add tx-22"></i>
        <span class="menu-item-label">Verifikasi User</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    <a href="{{ route('verifikasi-sk') }}" class="br-menu-link">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-ios-book-outline tx-24"></i>
        <span class="menu-item-label">Verifikasi SK</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    <label class="sidebar-label pd-x-15 mg-t-20">System Setting</label>
    <a href="#" class="br-menu-link">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-ios-gear-outline tx-24"></i>
        <span class="menu-item-label">Setting & Master</span>
        <i class="menu-item-arrow fa fa-angle-down"></i>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    <ul class="br-menu-sub nav flex-column">
      <li class="nav-item">
        <a href="{{ route('master-ukd.index') }}" class="nav-link">Master UKD</a>
      </li>
      <li class="nav-item">
        <a href="{{ route('petugas.index') }}" class="nav-link">Manajemen Petugas</a>
      </li>
      <li class="nav-item">
        <a href="{{ route('change-password') }}" class="nav-link">Ganti Password</a>
      </li>
    </ul>
    <label class="sidebar-label pd-x-15 mg-t-20">Modul Laporan</label>
    <a href="{{ route('report') }}" class="br-menu-link">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-clipboard tx-22"></i>
        <span class="menu-item-label">Laporan</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    @endcan
    @can('isUser')
    <label class="sidebar-label pd-x-15 mg-t-20">Ganti Password</label>
    <a href="{{ route('change-password') }}" class="br-menu-link">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-person-add tx-22"></i>
        <span class="menu-item-label">Ganti Password</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
    @endcan
    <a href="#" class="br-menu-link">
      <div class="br-menu-item">
        <i class="menu-item-icon icon ion-ios-list-outline tx-22"></i>
        <span class="menu-item-label">Bantuan</span>
      </div><!-- menu-item -->
    </a><!-- br-menu-link -->
  </div><!-- br-sideleft-menu -->

  <br>
</div><!-- br-sideleft -->
<!-- ########## END: LEFT PANEL ########## -->