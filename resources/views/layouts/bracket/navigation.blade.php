<!-- ########## START: HEAD PANEL ########## -->
<div class="br-header">
  <div class="br-header-left">
    <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
    <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
  </div><!-- br-header-left -->
  <div class="br-header-right">
    <nav class="nav">
      <div class="dropdown">
        <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
          <span class="logged-name hidden-md-down">{{ Auth::user()->name }}</span>
          @php 
            $nik = Auth::user()->nik;
            if($nik){
              $photo = "http://sik-lama.dephub.go.id/photo/{$nik}.jpg";
              $resp = get_headers($photo, 1);
              $needle   = "404";

              if( strpos( $resp[0], $needle ) !== false) {
                  $pic = "img/contact.png";
              }else{
                $pic = $photo;
              }
            }else{
              $pic = "img/contact.png";
            }
          @endphp
          <img src="{{ $pic }}" class="wd-32 rounded-circle" alt="" width="100%">
          <span class="square-10 bg-success"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-header wd-200">
          <ul class="list-unstyled user-profile-nav">
            <li><a href=""><i class="icon ion-ios-person"></i> Edit Profile</a></li>
            <li><a href=""><i class="icon ion-ios-gear"></i> Settings</a></li>
            <li>
              <a class="nav-link" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();" title="Logout">
                  <i class="icon ion-power"></i> Sign Out
              </a>
            </li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </ul>
        </div><!-- dropdown-menu -->
      </div><!-- dropdown -->
    </nav>
  </div><!-- br-header-right -->
</div><!-- br-header -->
<!-- ########## END: HEAD PANEL ########## -->