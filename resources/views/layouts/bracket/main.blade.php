<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layouts.bracket.head')
    @yield('style')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

  </head>

  <body>

    @include('layouts.bracket.sidebar')

    @include('layouts.bracket.navigation')

    @yield('content')

    @include('layouts.bracket.script')

    @yield('script')
  </body>
</html>
