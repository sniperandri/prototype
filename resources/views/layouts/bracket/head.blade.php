<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Himpunan Evaluasi Pendidikan Indonesia - Enhancing Quality Education Through Quality Evaluation">
    <meta name="author" content="ThemePixels">
	<link rel="shortcut icon" href="{{ asset('img/dephub.png') }}">
    <title>{{ config('app.name', 'HIMPUNAN EVALUASI PENDIDIKAN INDONESIA') }}</title>

    <!-- vendor css -->
    <link href="{{ asset('bracket/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('bracket/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('bracket/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('bracket/jquery-switchbutton/jquery.switchButton.css') }}" rel="stylesheet">
    <link href="{{ asset('bracket/rickshaw/rickshaw.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bracket/chartist/chartist.css') }}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{ asset('bracket/css/bracket.css') }}">
