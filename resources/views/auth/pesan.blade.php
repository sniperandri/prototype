@extends('layouts.registrasi')
@section('content')
    <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
      <div class="login-wrapper wd-500 wd-xs-700 pd-25 pd-xs-40 bg-white rounded shadow-base">
        <div class="signin-logo tx-16 tx-bold tx-black"><img src="{{ asset('img/logohepi.png') }}" width="64" style="float:left;"/> <br />
        </div>
        <br>
        <div class="alert alert-info alert-bordered pd-y-20" role="alert">
            <div class="d-sm-flex align-items-center justify-content-start">
                <i class="icon ion-ios-information alert-icon tx-52 mg-r-20"></i>
                <div class="mg-t-20 mg-sm-t-0">
                    <h5 class="mg-b-2">Terima kasih telah melakukan registrasi user, silahkan cek email Anda untuk melakukan verifikasi email</h5>
                    <p class="mg-b-0 tx-gray"><a href="/login">klik di sini</a> untuk kembali ke halaman utama</p>
                </div>
            </div><!-- d-flex -->
        </div><!-- alert -->
      </div><!-- login-wrapper -->
    </div><!-- d-flex -->
@endsection