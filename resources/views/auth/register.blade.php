@extends('layouts.registrasi')

@section('content')
    <style>
        #tombol{
            cursor: pointer;
        }
    </style>
    <div class="d-flex align-items-center justify-content-center bg-br-primary ht-500v">

      <div class="login-wrapper wd-500 wd-xs-700 pd-25 pd-xs-40 bg-white rounded shadow-base">
            <div class="signin-logo tx-16 tx-bold tx-black"><img src="{{ asset('img/logohepi.png') }}" width="128"/> <br />
        </div>
        <br>
        <h4>FORM REGISTRASI</h4>
        @include('layouts._flash')
        {!! Form::open(['url' => '/register','method'=>'post', 'files'=>TRUE]) !!}
            <div class="form-group">
                <div class="row">
                    <div class="col form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                        {!! Form::label('name', 'Nama Lengkap') !!}
                        <span style="color:red"> *</span>
                        {!! Form::text('name', null, ['class'=>'form-control','placeholder' => 'Masukkan Nama Lengkap']) !!}
                        @if($errors->has('name'))
                            <span class="badge badge-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                        {!! Form::label('email', 'Email') !!}
                        <span style="color:red"> *</span>
                        {!! Form::text('email', null, ['class'=>'form-control','placeholder' => 'Masukkan Email Anda']) !!}
                        @if($errors->has('email'))
                            <span class="badge badge-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="col form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                        {!! Form::label('password', 'Password') !!}
                        <span style="color:red"> *</span>
                        {!! Form::password('password', ['class'=>'form-control','placeholder' => 'Masukkan Password (Minimal 6 karakter)']) !!}
                        @if($errors->has('password'))
                            <span class="badge badge-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col form-group {{ $errors->has('jenis_identitas') ? 'has-error' : ''}}">
                        {!! Form::label('jenis_identitas', 'Jenis Identitas') !!}
                        <span style="color:red"> *</span>
                        {!! Form::select('jenis_identitas', ['KTP' => 'KTP', 'SIM' => 'SIM', 'Paspor' => 'Paspor'], null, ['placeholder' => 'Pilih Jenis Identitas', 'class' => 'form-control']) !!}
                        @if($errors->has('jenis_identitas'))
                            <span class="badge badge-danger">{{ $errors->first('jenis_identitas') }}</span>
                        @endif
                    </div>
                    <div class="col form-group {{ $errors->has('no_identitas') ? 'has-error' : ''}}">
                        {!! Form::label('no_identitas', 'Nomor Identitas:'); !!} <span style="color:red"> *</span>
                        {!! Form::text('no_identitas', null, ['class'=>'form-control','placeholder' => 'Masukkan Nomor Identitas']) !!}
                        @if($errors->has('no_identitas'))
                            <span class="badge badge-danger">{{ $errors->first('no_identitas') }}</span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col form-group {{ $errors->has('pendidikan_terakhir') ? 'has-error' : ''}}">
                        {!! Form::label('pendidikan_terakhir', 'Pendidikan Terakhir') !!}
                        <span style="color:red"> *</span>
                        {!! Form::select('pendidikan_terakhir', ['Diploma' => 'Diploma', 'Sarjana' => 'Sarjana', 'Magister' => 'Magister', 'Doktor' => 'Doktor'], null, ['placeholder' => 'Pilih Pendidikan Terakhir', 'class' => 'form-control']) !!}
                        @if($errors->has('pendidikan_terakhir'))
                            <span class="badge badge-danger">{{ $errors->first('pendidikan_terakhir') }}</span>
                        @endif
                    </div>
                    <div class="col form-group {{ $errors->has('unit') ? 'has-error' : ''}}">
                        {!! Form::label('unit', 'Unit Kerja') !!}
                        <span style="color:red"> *</span>
                        {!! Form::select('unit', App\MasterUkd::orderBy('nama_ukd')->pluck('nama_ukd','kode_ukd'), null, ['placeholder' => 'Pilih Unit Kerja', 'class' => 'form-control']) !!}
                        @if($errors->has('unit'))
                            <span class="badge badge-danger">{{ $errors->first('unit') }}</span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col form-group {{ $errors->has('file_identitas') ? 'has-error' : ''}}">
                        {!! Form::label('file_identitas', 'Upload Identitas Diri '); !!} <span style="color:red"> *</span>
                        {!! Form::file('file_identitas', ['class' => 'form-control']); !!}
                        @if($errors->has('file_identitas'))
                        <span class="badge badge-danger">{{ $errors->first('file_identitas') }}</span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col form-group {{ $errors->has('file_ijazah') ? 'has-error' : ''}}">
                        {!! Form::label('file_ijazah', 'Upload Ijazah Terakhir '); !!} <span style="color:red"> *</span>
                        {!! Form::file('file_ijazah', ['class' => 'form-control']); !!}
                        @if($errors->has('file_ijazah'))
                        <span class="badge badge-danger">{{ $errors->first('file_ijazah') }}</span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col form-group {{ $errors->has('file_foto') ? 'has-error' : ''}}">
                        {!! Form::label('file_foto', 'Upload Pas Foto '); !!} <span style="color:red"> *</span>
                        {!! Form::file('file_foto', ['class' => 'form-control']); !!}
                        @if($errors->has('file_foto'))
                        <span class="badge badge-danger">{{ $errors->first('file_foto') }}</span>
                        @endif
                    </div>
                </div>
            </div><!-- form-group -->


            <span style="color:red">*</span>) Wajib Diisi <br />
            {{ Form::hidden('role', "PJ", ['class'=>'form-control']) }}
            {!! Form::submit('Register',['class'=>'btn btn-info btn-block', 'id'=>'tombol']) !!}
        {!! Form::close() !!}

        <div class="mg-t-40 tx-center">Sudah memiliki akun? <a href="{{ route('login') }}" class="tx-info">Login</a></div>
      </div><!-- login-wrapper -->
    </div><!-- d-flex -->
@endsection
@section('script')
    $(function(){
      'use strict';

      $('.select2').select2({
        minimumResultsForSearch: Infinity
      });
    });
@endsection
