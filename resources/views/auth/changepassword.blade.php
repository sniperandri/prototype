@extends('layouts.bracket.main')
@section('style')
<style>
    .br-footer {
        position: fixed;
        left: 1000;
        bottom: 0;
        width: 100%;
    }
    #tombol{
        cursor: pointer;
    }
</style>
@endsection

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">SK MASA BERLAYAR</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA.</p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Ganti Password</h6>

        <div class="form-layout form-layout-1">
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        {!! Form::open(['route' => 'change-password','method'=>'post']) !!}
        @csrf
            <div class="row mg-b-25">
                <div class="col-lg-12">
                    <div class="form-group mg-b-10-force {{ $errors->has('current-password') ? 'has-error' : ''}}">
                        {!! Form::label('current-password', 'Password Sekarang :'); !!}
                        {!! Form::password('current-password', ['class'=>'form-control','placeholder'=>'Masukkan Password Anda Saat Ini']); !!}
                        @if($errors->has('current-password'))
                            <span class="badge badge-danger">{{ $errors->first('current-password') }}</span>
                        @endif
                    </div>
                </div><!-- col-12 -->
                <div class="col-lg-12">
                    <div class="form-group mg-b-10-force {{ $errors->has('new-password') ? 'has-error' : ''}}">
                        {!! Form::label('new-password', 'Password Baru :'); !!}
                        {!! Form::password('new-password', ['class'=>'form-control','placeholder'=>'Input Password Baru']); !!}
                        @if($errors->has('new-password'))
                            <span class="badge badge-danger">{{ $errors->first('new-password') }}</span>
                        @endif
                    </div>
                </div><!-- col-12 -->
                <div class="col-lg-12">
                    <div class="form-group mg-b-10-force {{ $errors->has('new-password_confirmation') ? 'has-error' : ''}}">
                        {!! Form::label('new-password_confirmation', 'Konfirmasi Password Baru :'); !!}
                        {!! Form::password('new-password_confirmation', ['class'=>'form-control','placeholder'=>'Konfirmasi Password Baru Anda']); !!}
                        @if($errors->has('new-password_confirmation'))
                            <span class="badge badge-danger">{{ $errors->first('new-password_confirmation') }}</span>
                        @endif
                    </div>
                </div><!-- col-12 -->
            </div><!-- row -->
            {!! Form::submit('Ganti Password',['class'=>'btn btn-info', 'id'=>'tombol']) !!}
        {!! Form::close() !!}
        </div><!-- form-layout -->
    </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
</div>
@endsection
