@extends('layouts.login')

@section('content')
    <div class="row no-gutters flex-row-reverse ht-100v">
      <div class="col-md-6 bg-gray-200 d-flex align-items-center justify-content-center">
        <div class="login-wrapper wd-250 wd-xl-350 mg-y-30">
          <div class="container">
            <div class="row" align="center">
                <img src="{{ asset('/img/logohepi2.png') }}" width="128px;" style="margin: 20px auto;"/> <br>
            </div>
          </div>
            
          
          <form method="POST" action="{{ route('login') }}">
              @csrf
              <div class="form-group">
                  <input placeholder="Masukkan Email Anda" id="login" class="form-control {{ $errors->has('nik') || $errors->has('seafarer_code') ? ' is-invalid' : '' }}" name="login" value="{{ old('nik') ?: old('seafarer_code') }}" required autofocus>

                  @if($errors->has('seafarer_code') || $errors->has('nip'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('seafarer_code') ?: $errors->first('nip') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="form-group">
                  <input placeholder="Masukkan Password" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>
              <button type="submit" class="btn btn-info btn-block" style="cursor: pointer;">
                  {{ __('Login') }}
              </button>
          </form>
          <div class="mg-t-60 tx-center">Anda belum terdaftar? <a href="/register" class="tx-info">Registrasi</a></div>
        </div><!-- login-wrapper -->
      </div><!-- col -->
      <div class="col-md-6 bg-br-primary d-flex align-items-center justify-content-center">
        <div class="wd-250 wd-xl-450 mg-y-30">
          <div class="signin-logo tx-28 tx-bold tx-white"><span class="tx-normal"></span> Selamat <span class="tx-info">Datang</span> <span class="tx-normal"></span></div>
          

          <p class="tx-white-6">
            Silahkan Melakukan Login dan Registrasi untuk masuk ke dalam sistem. 
          </p>

        </div><!-- wd-500 -->
      </div>
    </div><!-- row -->
@endsection
