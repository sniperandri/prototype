@extends('layouts.bracket.main')
@section('style')
<style>
    .br-footer {
        position: fixed;
        left: 1000;
        bottom: 0;
        width: 100%;
    }
    #tombol{
        cursor: pointer;
    }
</style>
@endsection

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">SK MASA BERLAYAR</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA.</p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Input Data Sertifikat</h6>

        <div class="form-layout form-layout-1">
        {!! Form::open(['route' => 'submit-sertifikat','method'=>'post','files'=>TRUE]) !!}
        @csrf
            <div class="row mg-b-25">
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('buku_pelaut') ? 'has-error' : ''}}">
                        {!! Form::label('buku_pelaut', 'Nomor Buku Pelaut :'); !!}
                        {!! Form::text('buku_pelaut', '',['class'=>'form-control','placeholder'=>'Input Nomor Buku Pelaut Pada Sertifikat']); !!}
                        @if($errors->has('buku_pelaut'))
                            <span class="badge badge-danger">{{ $errors->first('buku_pelaut') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('buku_saku') ? 'has-error' : ''}}">
                        {!! Form::label('buku_saku', 'Nomor Buku Saku (Cadet) :'); !!}
                        {!! Form::text('buku_saku', '',['class'=>'form-control','placeholder'=>'Input Nomor Buku Saku (Jika Ada)']); !!}
                        @if($errors->has('buku_saku'))
                            <span class="badge badge-danger">{{ $errors->first('buku_saku') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('sertifikat') ? 'has-error' : ''}}">
                        {!! Form::label('sertifikat', 'Sertifikat Keahlian/Keterampilan :'); !!}
                        {!! Form::text('sertifikat', '',['class'=>'form-control','placeholder'=>'Input Sertifikat Keahlian/Keterampilan']); !!}
                        @if($errors->has('sertifikat'))
                            <span class="badge badge-danger">{{ $errors->first('sertifikat') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
            </div><!-- row -->
            {{ Form::hidden('id', $id, ['class'=>'form-control']) }}
            {!! Form::submit('Tambahkan Data Sertifikat',['class'=>'btn btn-info', 'id'=>'tombol']) !!}
        {!! Form::close() !!}
        </div><!-- form-layout -->
    </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
</div>
@endsection
