@extends('layouts.bracket.main')
@section('style')
<style>
    .br-footer {
        position: fixed;
        left: 1000;
        bottom: 0;
        width: 100%;
    }
    #tombol{
        cursor: pointer;
    }
</style>
@endsection

@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">SK MASA BERLAYAR</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA.</p>
</div>

<div class="br-pagebody">
    <div class="br-section-wrapper">
        <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Input Masa Layar</h6>

        <div class="form-layout form-layout-1">
        {!! Form::open(['route' => 'submit-masa-layar','method'=>'post','files'=>TRUE]) !!}
        @csrf
            <div class="row mg-b-25">
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('nama_kapal') ? 'has-error' : ''}}">
                        {!! Form::label('nama_kapal', 'Nama Kapal :'); !!}
                        {!! Form::text('nama_kapal', '',['class'=>'form-control','placeholder'=>'Input Nama Kapal']); !!}
                        @if($errors->has('nama_kapal'))
                            <span class="badge badge-danger">{{ $errors->first('nama_kapal') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('tonase_kotor') ? 'has-error' : ''}}">
                        {!! Form::label('tonase_kotor', 'GT Kapal :'); !!}
                        {!! Form::text('tonase_kotor', '',['class'=>'form-control','placeholder'=>'Input GT Kapal']); !!}
                        @if($errors->has('tonase_kotor'))
                            <span class="badge badge-danger">{{ $errors->first('tonase_kotor') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('daya') ? 'has-error' : ''}}">
                        {!! Form::label('daya', 'Tenaga Penggerak (KW) :'); !!}
                        {!! Form::text('daya', '',['class'=>'form-control','placeholder'=>'Input Tenaga Penggerak Kapal']); !!}
                        @if($errors->has('daya'))
                            <span class="badge badge-danger">{{ $errors->first('daya') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('daerah_layan') ? 'has-error' : ''}}">
                        {!! Form::label('daerah_layan', 'Daerah Pelayanan :'); !!}
                        {!! Form::text('daerah_layan', '',['class'=>'form-control','placeholder'=>'Input Daerah Pelayanan']); !!}
                        @if($errors->has('daerah_layan'))
                            <span class="badge badge-danger">{{ $errors->first('daerah_layan') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('jabatan') ? 'has-error' : ''}}">
                        {!! Form::label('jabatan', 'Jabatan :'); !!}
                        {!! Form::text('jabatan', '',['class'=>'form-control','placeholder'=>'Input Jabatan']); !!}
                        @if($errors->has('jabatan'))
                            <span class="badge badge-danger">{{ $errors->first('jabatan') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('tgl_naik') ? 'has-error' : ''}}">
                        {!! Form::label('tgl_naik', 'Tanggal Naik :'); !!}
                        {!! Form::date('tgl_naik', '',['class'=>'form-control']); !!}
                        @if($errors->has('tgl_naik'))
                            <span class="badge badge-danger">{{ $errors->first('tgl_naik') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('tgl_turun') ? 'has-error' : ''}}">
                        {!! Form::label('tgl_turun', 'Tanggal Turun :'); !!}
                        {!! Form::date('tgl_turun', '',['class'=>'form-control']); !!}
                        @if($errors->has('tgl_turun'))
                            <span class="badge badge-danger">{{ $errors->first('tgl_turun') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                    <div class="form-group mg-b-10-force {{ $errors->has('no_buku_pelaut') ? 'has-error' : ''}}">
                        {!! Form::label('no_buku_pelaut', 'Nomor Buku Pelaut :'); !!}
                        {!! Form::text('no_buku_pelaut', '',['class'=>'form-control','placeholder'=>'Input Nomor Buku Pelaut']); !!}
                        @if($errors->has('no_buku_pelaut'))
                            <span class="badge badge-danger">{{ $errors->first('no_buku_pelaut') }}</span>
                        @endif
                    </div>
                </div><!-- col-4 -->
            </div><!-- row -->
            {{ Form::hidden('seafarer_code', $code, ['class'=>'form-control']) }}
            {!! Form::submit('Tambahkan Masa Layar',['class'=>'btn btn-info', 'id'=>'tombol']) !!}
        {!! Form::close() !!}
        </div><!-- form-layout -->
    </div><!-- br-section-wrapper -->
</div><!-- br-pagebody -->
</div>
@endsection
