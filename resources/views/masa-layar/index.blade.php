@extends('layouts.bracket.main')
@section('content')
<div class="br-mainpanel content">
<div class="pd-30">
    <h4 class="tx-gray-800 mg-b-5">INFORMASI MASA LAYAR</h4>
    <p class="mg-b-0">HIMPUNAN EVALUASI PENDIDIKAN INDONESIA</p>
</div>

<div class="br-pagebody">
<div class="br-section-wrapper">
  @include('layouts._flash')
  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Informasi Pelaut</h6>

  <div class="form-layout form-layout-1">
    <div class="row mg-b-25">
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Nomor Permohonan: </label>
          <input class="form-control" type="text" name="firstname" placeholder="{{ $permohonan['nomor_permohonan'] }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
      <div class="col-lg-8">
        <div class="form-group">
          <label class="form-control-label">Keperluan: </label>
          <input class="form-control" type="text" name="lastname" placeholder="{{ $permohonan['keperluan'] }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Nama: </label>
          <input class="form-control" type="text" name="firstname" placeholder="{{ $pelaut['nama'] }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Tempat Lahir: </label>
          <input class="form-control" type="text" name="lastname" placeholder="{{ $pelaut['tempat_lahir'] }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
      <div class="col-lg-4">
        <div class="form-group">
          <label class="form-control-label">Tanggal Lahir: </label>
          <input class="form-control" type="text" name="email" placeholder="{{ dateTimeToDate($pelaut['tanggal_lahir']) }}" disabled="disabled">
        </div>
      </div><!-- col-4 -->
    </div><!-- row -->

    <div class="bd bd-gray-300 rounded table-responsive">
        <table class="table table-striped mg-b-0">
            <thead>
                <tr>
                    <th>Nama Sertifikat</th>
                    <th>Jenis Sertifikat</th>
                    <th class="text-center">Nomor Sertifikat</th>
                    <th class="text-center">Kode Blanko</th>
                    <th class="text-center">Tanggal Berlaku</th>
                    <th class="text-center">Tanggal Berakhir</th>
                </tr>
            </thead>
            <tbody>
              @foreach($pelaut['sertifikat'] as $sertifikat)
                <tr>
                    <td>{{ $sertifikat['nama'] }}</td>
                    <td>{{ $sertifikat['jenis'] }}</td>
                    <td align="center">{{ $sertifikat['no_sertifikat'] }}</td>
                    <td align="center">{{ $sertifikat['kode_blanko'] }}</td>
                    <td align="center">{{ $sertifikat['tgl_mulai'] }}</td>
                    <td align="center">{{ $sertifikat['tgl_akhir'] }}</td>
                </tr>
              @endforeach
            </tbody>
        </table>
    </div>

    <br>
  </div><!-- form-layout -->
</div>
<div class="br-section-wrapper">
        <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-b-10">Daftar Masa Berlayar</h6>
        <div class="d-flex">
            <div class="wd-50p wd-md-50p wd-lg-50p">
            </div>
            <div class="wd-50p wd-md-50p wd-lg-50p">
                <a href="{{ route('form-masa-layar',$permohonan->seafarer_code) }}" class="btn btn-primary bd-1 tx-uppercase tx-bold tx-10 mg-b-10 pull-right"><i class="fa fa-plus-circle"></i> Tambah Masa Layar</a>
            </div>
        </div>
        <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Kapal</th>
                        <th>GT</th>
                        <th>KW</th>
                        <th>Daerah Pelayanan</th>
                        <th>Jabatan</th>
                        <th>Tanggal Naik</th>
                        <th>Tanggal Turun</th>
                        <th>Masa Berlayar</th>
                    </tr>
                </thead>
		<tbody>
                    <?php $no = 1;?>
		    @foreach($buku_pelaut as $masalayar)
			<?php $masalayar = (Object) $masalayar; ?>
                    <tr>
                        <th scope="row">{{ $no }}</th>
                        <td>{{ $masalayar->NAMA_KAPAL }}</td>
                        <td>{{ $masalayar->TONASE_KOTOR }}</td>
                        <td>{{ $masalayar->DAYA }}</td>
                        <td>{{ $masalayar->DAERAH_PELAYARAN }}</td>
                        <td>{{ $masalayar->JABATAN }}</td>
                        <td>{{ $masalayar->TGL_NAIK }}</td>
                        <td>{{ $masalayar->TGL_TURUN }}</td>
                        <td>{{ datediff($masalayar->TGL_NAIK,$masalayar->TGL_TURUN) }}
                        </td>
                    <?php $no++;?>
                    </tr>
                    @endforeach

                    @foreach($permohonans as $masalayar)
                    <tr>
                        <th scope="row">{{ $no }}</th>
                        <td>{{ $masalayar->nama_kapal }}</td>
                        <td>{{ $masalayar->tonase_kotor }}</td>
                        <td>{{ $masalayar->daya }}</td>
                        <td>{{ $masalayar->daerah_layan }}</td>
                        <td>{{ $masalayar->jabatan }}</td>
                        <td>{{ $masalayar->tgl_naik }}</td>
                        <td>{{ $masalayar->tgl_turun }}</td>
                        <td>{{ datediff($masalayar->tgl_naik,$masalayar->tgl_turun) }}
                        </td>
                    <?php $no++;?>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <br>
        <div class="form-layout-footer">
            <a href="{{ route('form-sertifikat',$permohonan->id) }}" class="btn btn-primary active"><i class="fa fa-edit"></i> {{ $permohonan->status == 2 ? 'Tambah Data Sertifikat' : 'Update Data Sertifikat' }}</a>
            <a href="{{ route('print-pdf',$permohonan->id) }}" class="btn btn-warning active" target="_blank"><i class="fa fa-search"></i> {{ $permohonan->status == 2 ? 'SK Masa Layar' : 'Preview SK Masa Layar' }}</a>
            @if($permohonan->status !=2 && $permohonan->status !=4)
            <a href="{{ route('permohonan.approve-sk',$permohonan->id) }}" class="btn btn-success active"><i class="fa fa-check"></i> Approve Permohonan</a>
            <a href="{{ route('permohonan.reject',$permohonan->id) }}" class="btn btn-danger active"><i class="fa fa-times"></i> Tolak Permohonan</a>
            @endif
        </div><!-- form-layout-footer -->
    </div><!-- br-section-wrapper -->
</div>
@include('layouts.bracket.footer')
@endsection
