<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* Change Password */
Route::get('/change-password','HomeController@showChangePasswordForm');
Route::post('/change-password','HomeController@changePassword')->name('change-password');

/* Registrasi */
Route::get('register', 'RegisterController@index')->name('register');
Route::post('register', 'RegisterController@store');
Route::get('register/pesan', 'RegisterController@pesan')->name('pesan');
Route::get('register/ver/{id}', 'RegisterController@verifikasi')->name('verReg');
Route::get('register/sukses', 'RegisterController@sukses')->name('terimakasih');
Route::get('network-error', 'PrintController@network')->name('network-error');

/* Petugas */
Route::get('petugas', 'PetugasController@index')->middleware('can:isPetugas','auth')->name('petugas.index');
Route::get('form-petugas', 'PetugasController@create')->middleware('can:isPetugas','auth')->name('petugas.create');
Route::post('form-petugas', 'PetugasController@store')->middleware('can:isPetugas','auth')->name('submit-petugas');
Route::get('petugas/{id}','PetugasController@edit')->name('petugas.edit');
Route::put('petugas/{id}','PetugasController@update')->name('petugas.update');
Route::delete('petugas/{id}', 'PetugasController@destroy')->middleware('can:isPetugas','auth')->name('petugas.destroy');

Route::get('master-ukd', 'MasterUkdController@index')->middleware('can:isPetugas','auth')->name('master-ukd.index');
Route::get('form-master-ukd', 'MasterUkdController@create')->middleware('can:isPetugas','auth')->name('master-ukd.create');
Route::post('form-master-ukd', 'MasterUkdController@store')->middleware('can:isPetugas','auth')->name('submit-master-ukd');
Route::get('master-ukd/{id}','MasterUkdController@edit')->name('master-ukd.edit');
Route::put('master-ukd/{id}','MasterUkdController@update')->name('master-ukd.update');
Route::delete('master-ukd/{id}', 'MasterUkdController@destroy')->middleware('can:isPetugas','auth')->name('master-ukd.destroy');

/* Permohonan SK Masa Berlayar */
Route::get('cetak-kartu','PermohonanSkLayarController@index')->name('sk')->middleware('auth');
Route::get('form-permohonan', 'PermohonanSkLayarController@create')->name('form-sk')->middleware('auth');
Route::post('form-permohonan', 'PermohonanSkLayarController@store')->middleware('auth')->name('submit-permohonan');

/* Verifikasi */
Route::get('verifikasi-sk','VerifikasiController@verifikasiSk')->middleware('can:isPetugas','auth')->name('verifikasi-sk');
Route::get('verifikasi-user','VerifikasiController@verifikasiUser')->middleware('can:isPetugas','auth')->name('verifikasi-user');
Route::get('proses-permohonan/{id}','VerifikasiController@proses')->middleware('can:isPetugas','auth')->name('permohonan.proses');
Route::get('approval-permohonan/{id}','VerifikasiController@approve')->middleware('can:isPetugas','auth')->name('permohonan.approve');
Route::get('approval-permohonan-sk/{id}','VerifikasiController@approveSk')->middleware('can:isPetugas','auth')->name('permohonan.approve-sk');
Route::get('penolakan-permohonan/{id}','VerifikasiController@reject')->middleware('can:isPetugas','auth')->name('permohonan.reject');
Route::get('approval-user/{id}','VerifikasiController@approveUser')->middleware('can:isPetugas','auth')->name('user.approve');
Route::get('penolakan-user/{id}','VerifikasiController@rejectUser')->middleware('can:isPetugas','auth')->name('user.reject');

/* Masa Layar */
Route::get('form-masa-layar/{id}', 'MasaBerlayarController@create')->name('form-masa-layar')->middleware('auth');
Route::post('form-masa-layar', 'MasaBerlayarController@store')->middleware('auth')->name('submit-masa-layar');
Route::get('masa-layar/{id}','VerifikasiController@lihat')->middleware('auth')->name('permohonan.masa-layar');

/* Pre Sertifikat */
Route::get('form-sertifikat/{id}', 'MasaBerlayarController@sertifikat')->name('form-sertifikat')->middleware('auth');
Route::post('form-sertifikat', 'MasaBerlayarController@storeSertifikat')->middleware('auth')->name('submit-sertifikat');


/* Cetak Dokumen */
Route::get('pracetak-dokumen/{id}','PrintController@pracetak')->middleware('auth')->name('print.pracetak');

Route::get('qr-code','PrintController@qrcode')->name('qr-code');

Route::get('/downloadPDF/{id}','PrintController@downloadPDF')->name('print-pdf');
Route::get('/bukti-permohonan/{permohonan}','PrintController@bukti')->name('bukti-permohonan');

Route::get('/invoice', function () {
    $pdf = PDF::loadView('cetak.invoice');
    return $pdf->download('invoice.pdf');
})->name('cetak-invoice');

/* Report */
Route::get('laporan','LaporanController@index')->middleware('can:isPetugas','auth')->name('report');

/* PNBP */
Route::get('get-kode-billing','PnbpController@kodbil')->middleware('auth')->name('get-kode-billing');
Route::get('kode-billing/{permohonan}','PnbpController@getKodeBilling')->middleware('auth')->name('kode-billing');

/* Search  */
Route::get('/filter','LaporanController@search');
Route::get('/cari-permohonan','VerifikasiController@search');
Route::get('/cari-user','VerifikasiController@searchUser');